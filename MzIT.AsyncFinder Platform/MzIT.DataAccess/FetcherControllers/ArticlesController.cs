﻿using MzIT.BasicToolkits.Utility.FetcherHelpers;
using MzIT.DataAccess.EntityModels.DetectionSystemEntities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MzIT.DataAccess.FetcherControllers
{
    public class ArticlesController
    {
        #region Fields

        #endregion

        #region Helper

        /// <summary>
        /// Convert text to Persian text by changing Arabic characters to Persian
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ToFormatted(string input)
        {
            if (!string.IsNullOrWhiteSpace(input))
            {
                input = input.Replace('ي', 'ی');
                input = input.Replace('ك', 'ک');
                input = input.Replace('‌', ' ');
                input = input.Replace(',', '،');
            }

            return input;
        }

        public static List<string> GetInputFiles()
        {
            List<string> txtInputFiles = new List<string>();

            try
            {
                string articlesPath =
                    string.Format(
                        "{0}\\{1}",
                        BasicMethods.BaseDirectory,
                        BasicMethods.BasicExtraArticlesDirectory);

                txtInputFiles = Directory.EnumerateFiles(articlesPath, "*.txt")?.ToList();
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex); }

            return txtInputFiles;
        }

        #endregion

        #region Constructor_Definition

        private static async Task<bool> GetArticle(string inputPath, string searchValue)
        {
            try
            {
                var fileStream = new FileStream(inputPath, FileMode.Open, FileAccess.Read);
                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                {
                    Article article = new Article
                    {
                        InputPath = inputPath,
                        DisplayName = Path.GetFileNameWithoutExtension(inputPath),

                        Context = streamReader.ReadToEnd(),
                        Title = streamReader.ReadLine(),
                    };

                    article.FormattedContext = ToFormatted(article.Context);
                    article.FindKeyword(searchValue);

                    return true;
                }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex); }

            return false;
        }

        public static async Task<bool> GetArticles(List<string> txtInputPaths, string searchValue)
        {
            if (txtInputPaths.IsNullOrEmpty())
            { return false; }

            try
            {
                txtInputPaths.ForEach(i => GetArticle(i, searchValue));

                return true;
            }
            catch (Exception ex)
            {
                FetcherTryCatch.FTC_GetCEM_Error(ex);
                return false;
            }
        }

        #endregion
    }
}
