﻿namespace MzIT.DataAccess.EntityModels.DetectionSystemEntities
{
    public class Keyword
    {
        public string Key { get; set; }
        public int Count { get; set; }
    }
}
