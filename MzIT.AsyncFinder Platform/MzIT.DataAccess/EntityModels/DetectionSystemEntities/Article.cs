﻿using MzIT.BasicToolkits.Utility.FetcherHelpers;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.RegularExpressions;

namespace MzIT.DataAccess.EntityModels.DetectionSystemEntities
{
    public class Article
    {
        /// <summary>
        /// Path of file
        /// </summary>
        public string InputPath { get; set; }
        /// <summary>
        /// Name of file
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Store ReadToEnd of input file
        /// </summary>
        public string Context { get; set; }
        /// <summary>
        /// Set first line for some display feature
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Store ReadToEnd of input file after normalize some things
        /// </summary>
        public string FormattedContext { get; set; }

        public Keyword Keyword { get; set; }

        #region GlobalizationDisplayProperties.DisplayProperty_NotMapped_IsReadOnly

        [NotMapped]
        public bool IsMatch => (Keyword?.Count > 0);

        #endregion

        #region Methods

        public override string ToString()
        {
            return $"DisplayName: {DisplayName}; Count: {Keyword?.Count};";
        }

        public void FindKeyword(string searchKey)
        {
            if (string.IsNullOrWhiteSpace(searchKey) || string.IsNullOrWhiteSpace(FormattedContext))
            { return; }

            Keyword = new Keyword
            {
                Key = searchKey,
                Count = Regex.Matches(FormattedContext, searchKey).Count
            };

            if (IsMatch)
            { Logging.LogAction(ToString(), true); }
        }

        #endregion
    }
}
