﻿namespace System.ComponentModel
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public class SpecialGuidAttribute : Attribute
    {
        protected string SpecialGuidValue { get; set; }

        public static readonly SpecialGuidAttribute Default = new SpecialGuidAttribute();

        public SpecialGuidAttribute() : this(string.Empty) { }

        public SpecialGuidAttribute(string especialGuid) => SpecialGuidValue = especialGuid;

        public override int GetHashCode() => SpecialGuid.GetHashCode();

        public override bool IsDefaultAttribute() => Equals(Default);

        public virtual string SpecialGuid => SpecialGuidValue;

        public override bool Equals(object obj) => (obj == this) || ((obj is SpecialGuidAttribute attribute) && (attribute.SpecialGuid == SpecialGuid));
    }
}