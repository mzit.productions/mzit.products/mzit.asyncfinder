﻿namespace System.ComponentModel
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public class SpecialDescriptionAttribute : Attribute
    {
        protected string SpecialDescriptionValue { get; set; }

        public static readonly SpecialDescriptionAttribute Default = new SpecialDescriptionAttribute();

        public SpecialDescriptionAttribute() : this(string.Empty) { }

        public SpecialDescriptionAttribute(string especialDescription) => SpecialDescriptionValue = especialDescription;

        public override int GetHashCode() => SpecialDescription.GetHashCode();

        public override bool IsDefaultAttribute() => Equals(Default);

        public virtual string SpecialDescription => SpecialDescriptionValue;

        public override bool Equals(object obj) => (obj == this) || ((obj is SpecialDescriptionAttribute attribute) && (attribute.SpecialDescription == SpecialDescription));
    }
}