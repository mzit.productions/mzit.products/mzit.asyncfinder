﻿namespace System.ComponentModel
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public class SpecialViewModelAttribute : Attribute
    {
        protected string SpecialViewModelValue { get; set; }

        public static readonly SpecialViewModelAttribute Default = new SpecialViewModelAttribute();

        public SpecialViewModelAttribute() : this(string.Empty) { }

        public SpecialViewModelAttribute(string especialViewModel) => SpecialViewModelValue = especialViewModel;

        public override int GetHashCode() => SpecialViewModel.GetHashCode();

        public override bool IsDefaultAttribute() => Equals(Default);

        public virtual string SpecialViewModel => SpecialViewModelValue;

        public override bool Equals(object obj) => (obj == this) || ((obj is SpecialViewModelAttribute attribute) && (attribute.SpecialViewModel == this.SpecialViewModel));
    }
}