﻿namespace System.ComponentModel
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public class SpecialColorAttribute : Attribute
    {
        protected string SpecialColorValue { get; set; }

        public static readonly SpecialColorAttribute Default = new SpecialColorAttribute();

        public SpecialColorAttribute() : this(string.Empty) { }

        public SpecialColorAttribute(string especialColor) => SpecialColorValue = especialColor;

        public override int GetHashCode() => SpecialColor.GetHashCode();

        public override bool IsDefaultAttribute() => Equals(Default);

        public virtual string SpecialColor => SpecialColorValue;

        public override bool Equals(object obj) => (obj == this) || ((obj is SpecialColorAttribute attribute) && (attribute.SpecialColor == this.SpecialColor));
    }
}