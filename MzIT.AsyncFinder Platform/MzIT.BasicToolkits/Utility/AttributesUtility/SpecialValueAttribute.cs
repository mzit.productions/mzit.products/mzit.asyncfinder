﻿namespace System.ComponentModel
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public class SpecialValueAttribute : Attribute
    {
        protected string SpecialValueValue { get; set; }

        public static readonly SpecialValueAttribute Default = new SpecialValueAttribute();

        public SpecialValueAttribute() : this(string.Empty) { }

        public SpecialValueAttribute(string especialValue) => SpecialValueValue = especialValue;

        public override int GetHashCode() => SpecialValue.GetHashCode();

        public override bool IsDefaultAttribute() => Equals(Default);

        public virtual string SpecialValue => SpecialValueValue;

        public override bool Equals(object obj) => (obj == this) || ((obj is SpecialValueAttribute attribute) && (attribute.SpecialValue == SpecialValue));
    }
}