﻿namespace System.ComponentModel
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public class SpecialViewAttribute : Attribute
    {
        protected string SpecialViewValue { get; set; }

        public static readonly SpecialViewAttribute Default = new SpecialViewAttribute();

        public SpecialViewAttribute() : this(string.Empty) { }

        public SpecialViewAttribute(string especialView) => SpecialViewValue = especialView;

        public override int GetHashCode() => SpecialView.GetHashCode();

        public override bool IsDefaultAttribute() => Equals(Default);

        public virtual string SpecialView => SpecialViewValue;

        public override bool Equals(object obj) => (obj == this) || ((obj is SpecialViewAttribute attribute) && (attribute.SpecialView == this.SpecialView));
    }
}