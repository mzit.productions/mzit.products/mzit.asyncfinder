﻿using System;
using System.IO;
using static MzIT.BasicToolkits.Utility.Entities.GME_SpecialMethods;

namespace MzIT.BasicToolkits.Utility.FetcherHelpers
{
    public static class GlobalizationFormats
    {
        #region DateTime Formats

        public const string ISO_DATETIME_FORMAT = "yyyy-MM-ddTHH^mm^ss";
        public const string ISO_DATETIME_FORMAT_Log = "yyyy-MM-ddTHH:mm:ss";
        public const string ISO_DATETIME_FORMAT_Post = "yyyy-MM-ddTHH:mm:ss";
        public const string ISO_DATETIME_FORMAT_Show = "yyyy-MM-dd HH:mm:ss";
        public const string ISO_DATETIME_FORMAT_ShowShort = "yyyy-MM-dd hh:mm tt";
        public const string ISO_DATETIME_FORMAT_ShowShort_Wide = "yyyy-MM-dd  hh:mm tt";
        public const string ISO_DATETIME_FORMAT_ShowShort_Plus = "yyyy-MM-dd hh:mm:ss tt";

        public const string ISO_DATE_FORMAT_Post = "yyyy-MM-dd";
        public const string ISO_DATE_FORMAT_Show = "yyyy-MM-dd";

        public const string ISO_TIME_FORMAT_Show = "HH:mm:ss";
        public const string ISO_TIME_FORMAT_ShowShort = "hh:mm tt";
        public const string ISO_TIME_FORMAT_ShowShort_Plus = "hh:mm:ss tt";

        public const string ISO_DATETIME_FORMAT_CZKEMClass = "yyyy-MM-dd HH:mm:ss";

        #endregion

        #region Number Formats

        public const string CurrencySymbol = "$";
        public const int CurrencyDecimalDigits = 2;
        public const string CurrencyDecimalDigitsFormat = "N2";
        public const string CurrencyDecimalSeparator = ".";
        public const string CurrencyGroupSeparator = ",";

        public const int NumberDecimalDigits = 2;
        public const string NumberDecimalSeparator = ".";
        public const string NumberGroupSeparator = ",";

        #endregion

        #region Special Formats

        /// <summary>
        /// Use for split multi parameter in one cell
        /// </summary>
        public const string SpliterCharacters = ",";
        public const string GroupSeparator = ",";

        public const string UnknownParameter = "--";
        public const string UnknownParameterPro = "NA";
        public const string DisableParameter = "--";
        public const string ListFollowingSections = "\n- ";
        public const string ListFollowingSectionsLite = "- ";
        public const string ListFollowingSectionsSpecial = "-";
        public const string LogicalDisplayMultiplication = "X ";
        public const string CurrencyDecimalDigitsFormatQuantity = "0.##";

        public const string DefaultPhotoFormat = "png";

        public const int LogicalInfinityValue = -1;
        /// <summary>
        /// Set the abnormal value to avoid duplicate values.
        /// </summary>
        public const int LogicalIsFailedValue = -1072;

        public const int QuantityShowFiltersItems = 10;

        public const int RemainingMode = 1000;

        public const int DefaultCountSeperatorPlus = 4;

        public const string DefaultCaloriesValue = "N/A";

        #endregion
    }

    public static class GlobalizationDisplayProperties
    {
        #region Comment DisplayProperties

        /// <summary>
        /// Display Property [NotMapped] - More Special(Local) IsReadOnly Model Entities
        /// #region GlobalizationDisplayProperties.DisplayProperty_NotMapped_IsReadOnly;
        /// </summary>
        public const string DisplayProperty_NotMapped_IsReadOnly = "NotMapped_IsReadOnly - More IsReadOnly Entities(details) in Special(Local) Model Entities";

        /// <summary>
        /// Display Property [NotMapped] - More Special(Local) Model Entities
        /// #region GlobalizationDisplayProperties.DisplayProperty_NotMapped;
        /// </summary>
        public const string DisplayProperty_NotMapped = "NotMapped - More Entities(details) in Special(Local) Model Entities";

        /// <summary>
        /// Display Property [ExtraFeatures] - More features and will be complete or upgrade in the future
        /// GlobalizationDisplayProperties.DisplayProperty_ExtraFeatures;
        /// </summary>
        public const string DisplayProperty_ExtraFeatures = "ExtraFeatures - More features and will be complete or upgrade in the future";

        /// <summary>
        /// Display Property [TemporarilyDisabled] - Please don't delete this comment in next line, will be activate if change policy! After a certain time, if not used, the code will/should be deleted. Thanks
        /// GlobalizationDisplayProperties.DisplayProperty_TemporarilyDisabled;
        /// <!--/// GlobalizationDisplayProperties.DisplayProperty_TemporarilyDisabled;-->
        /// </summary>
        public const string DisplayProperty_TemporarilyDisabled = "TemporarilyDisabled - Please don't delete this comment in next line, will be activate if change policy! After a certain time, if not used, the code will/should be deleted. Thanks";

        /// <summary>
        /// Display Property [DoSomething] - Should be continue in the last free time
        /// GlobalizationDisplayProperties.DisplayProperty_DoSomething;
        /// </summary>
        public const string DisplayProperty_DoSomething = "DoSomething - Should be continue in the last free time";

        /// <summary>
        /// Display Property [OnlyForTest] - To test in the develop state of app, Should be reverce after test
        /// GlobalizationDisplayProperties.DisplayProperty_OnlyForTest;
        /// </summary>
        public const string DisplayProperty_OnlyForTest = "OnlyForTest - To test in the develop state of app";

        /// <summary>
        /// Display Property [DisabledByRequest] - Disabled by request
        /// GlobalizationDisplayProperties.DisplayProperty_DisabledByRequest;
        /// </summary>
        public const string DisplayProperty_DisabledByRequest = "DisabledByRequest - Disabled by request";

        /// <summary>
        /// Display Property [XmlnsDefinition] - Just for assembly: XmlnsDefinition
        /// GlobalizationDisplayProperties.DisplayProperty_XmlnsDefinition;
        /// </summary>
        public const string DisplayProperty_XmlnsDefinition = "XmlnsDefinition - Just for assembly: XmlnsDefinition";

        /// <summary>
        /// Display Property [IsSetDefaultProperties] - More control to set just one time default properties
        /// GlobalizationDisplayProperties.DisplayProperty_IsSetDefaultProperties;
        /// </summary>
        public const string DisplayProperty_IsSetDefaultProperties = "IsSetDefaultProperties - More control to set just one time default properties";

        /// <summary>
        /// Display Property [IsSelectedTabItem] - More control to load some function of this user control just when is selected tabItem of tabControl
        /// GlobalizationDisplayProperties.DisplayProperty_IsSelectedTabItem;
        /// </summary>
        public const string DisplayProperty_IsSelectedTabItem = "IsSelectedTabItem - More control to load some function of this user control just when is selected tabItem of tabControl";

        /// <summary>
        /// Display Property [IsEditModeItem] - More control to StackPanelManageAllItems
        /// GlobalizationDisplayProperties.DisplayProperty_IsEditModeItem;
        /// </summary>
        public const string DisplayProperty_IsEditModeItem = "IsEditModeItem - More control to StackPanelManageAllItems";

        /// <summary>
        /// Display Property [IsFinishedCreateEditItem] - More control to CurrentItem in create/edit node
        /// GlobalizationDisplayProperties.DisplayProperty_IsFinishedCreateEditItem;
        /// </summary>
        public const string DisplayProperty_IsFinishedCreateEditItem = "IsFinishedCreateEditItem - More control to CurrentItem in create/edit node";

        /// <summary>
        /// Display Property [IsLiteModeItem] - Gets / Sets the Entity that the control in lite mode permission
        /// GlobalizationDisplayProperties.DisplayProperty_IsLiteModeItem;
        /// </summary>
        public const string DisplayProperty_IsLiteModeItem = "IsLiteModeItem - Gets / Sets the Entity that the control in lite mode permission";

        /// <summary>
        /// Display Property [SelectedItems] - Need for [get selected OR create and binding] items of list
        /// GlobalizationDisplayProperties.DisplayProperty_SelectedItems;
        /// </summary>
        public const string DisplayProperty_SelectedItems = "SelectedItems - Need for [get selected OR create and binding] items of list";

        /// <summary>
        /// Display Property [SelectionItems] - Gets / Sets the Entity that the control is showing and binding items of list
        /// GlobalizationDisplayProperties.DisplayProperty_SelectionItems;
        /// </summary>
        public const string DisplayProperty_SelectionItems = "SelectionItems - Gets / Sets the Entity that the control is showing and binding items of list";

        /// <summary>
        /// Display Property [DisplayContent] - Gets / Sets the Content that the control is showing and binding title in design
        /// GlobalizationDisplayProperties.DisplayProperty_DisplayContent;
        /// </summary>
        public const string DisplayProperty_DisplayContent = "DisplayContent - Gets / Sets the Content that the control is showing and binding title in design";

        /// <summary>
        /// Display Property [JustForTheViews] - Just for the views OR Just to refresh the data
        /// GlobalizationDisplayProperties.DisplayProperty_JustForTheViews;
        /// </summary>
        public const string DisplayProperty_JustForTheViews = "DisplayContent - Just for the views OR Just to refresh the data";

        #endregion

        #region Common DisplayProperties

        public const string DefaultAutomaticallyAddDescription = "This item was created automatically by the system!";

        public const string ErrorCaption_FileSave = "Error";
        public const string ErrorMessage_FileSave = "File Save Error \n\nUnable to save document! \nDo you wanna to retry?";

        public const string CloneDescription = "Duplicate";

        #endregion

        #region Special DisplayProperties

        #endregion
    }

    public static class Logging
    {
        #region Fields

        public static bool IsDebugMode { get; set; } = true;

        #endregion

        #region Methods

        public static void LogAction(string textLog, bool showTime)
        {
            try
            {
                Console.ForegroundColor = ConsoleColor.Yellow;

                if (showTime == true)
                { Console.WriteLine(DateTime.Now.ToString(GlobalizationFormats.ISO_DATETIME_FORMAT_Log) + ": " + textLog); }
                else
                { Console.WriteLine(textLog); }
            }
            catch (Exception) { }
        }

        public static void LogPrint(string textLog)
        {
            try
            {
                Console.ForegroundColor = ConsoleColor.Green;

                Console.WriteLine(DateTime.Now.ToString(GlobalizationFormats.ISO_DATETIME_FORMAT_Log) + ":" + textLog);
            }
            catch (Exception) { }
        }

        public static void LogEmail(string txt)
        {
            try
            {
                Console.ForegroundColor = ConsoleColor.Magenta;

                Console.WriteLine(DateTime.Now.ToString(GlobalizationFormats.ISO_DATETIME_FORMAT_Log) + ":" + txt);
            }
            catch (Exception) { }
        }

        public static void LogErrorForUser(string textLog, bool showTime)
        {
            try
            {
                Console.ForegroundColor = ConsoleColor.Red;

                if (showTime == true)
                { Console.WriteLine(DateTime.Now.ToString(GlobalizationFormats.ISO_DATETIME_FORMAT_Log) + ":" + textLog); }
                else
                { Console.WriteLine(textLog); }
            }
            catch (Exception) { }
        }

        public static void LogError(Exception ex, string friendlyMessage, string friendlyResponseMessage)
        {
            try
            {
                Console.ForegroundColor = ConsoleColor.Red;

                string textLog = string.Empty;

                if (!string.IsNullOrWhiteSpace(friendlyMessage))
                { textLog += "\n _FriendlyMessage: " + friendlyMessage.Trim(); }

                if (!string.IsNullOrWhiteSpace(friendlyResponseMessage))
                { textLog += "\n _FriendlyResponseMessage: " + friendlyResponseMessage.Trim(); }

                if (ex != null)
                {
                    if (ex.Message != null)
                    { textLog += "\n _Message: " + ex.Message.Trim(); }
                    if (!string.IsNullOrEmpty(ex.InnerException?.Message))
                    { textLog += "\n _Inner_Exception: " + ex.InnerException?.Message.Trim(); }
                    if (!string.IsNullOrEmpty(ex.StackTrace))
                    { textLog += "\n _Stack_Trace: " + ex.StackTrace.Trim(); }
                }

                if (!Logging.IsDebugMode)
                {
                    using (StreamWriter writer =
                        new StreamWriter(
                            new FileStream(
                                string.Format(
                                    "{0}\\{1}\\{2}",
                                    BasicMethods.BaseDirectory,
                                    BasicMethods.BasicExtraDataDirectory,
                                    SEE_ContentType.LogError.ToDescriptionString()), FileMode.Append)))
                    {
                        writer.WriteLine(
                            "{0}  Ver_{1}  {2}  {3}",
                            "Error",
                            BasicAssemblyAttributeAccessors.EntryAssemblyVersion,
                            DateTime.Now.ToString(GlobalizationFormats.ISO_DATETIME_FORMAT_Log),
                            textLog);
                    }
                }
                else
                {
                    using (StreamWriter writer =
                           new StreamWriter(
                               new FileStream(
                                   string.Format(
                                       @"D:\Errors_DebugMode.lebak"), FileMode.Append)))
                    {
                        writer.WriteLine(
                            "{0}  Ver_{1}  {2}  {3}",
                            "Error",
                            BasicAssemblyAttributeAccessors.EntryAssemblyVersion,
                            DateTime.Now.ToString(GlobalizationFormats.ISO_DATETIME_FORMAT_Log),
                            textLog);
                    }
                }

                LogAction(textLog, true);
            }
            catch (Exception) { }
        }

        public static void LogOutputInfo(string outputInfo)
        {
            try
            {
                Console.ForegroundColor = ConsoleColor.Blue;

                string textLog = string.Empty;
                if (!string.IsNullOrWhiteSpace(outputInfo))
                { textLog += outputInfo.Trim(); }

                using (StreamWriter writer =
                    new StreamWriter(
                        new FileStream(
                            string.Format(
                                "{0}\\{1}\\{2}",
                                BasicMethods.BaseDirectory,
                                BasicMethods.BasicExtraDataDirectory,
                                SEE_ContentType.OutputInfo.ToDescriptionString()), FileMode.Append)))
                {
                    writer.WriteLine(
                        "{0}  Ver_{1}  {2}  {3}",
                        "OutputInfo",
                        BasicAssemblyAttributeAccessors.EntryAssemblyVersion,
                        DateTime.Now.ToString(GlobalizationFormats.ISO_DATETIME_FORMAT_Log),
                        textLog);
                }

                LogAction(textLog, true);
            }
            catch (Exception) { }
        }

        public static void LogActionInfo(System.Diagnostics.EventLog oEventLog, string actionInfo)
        {
            try
            {
                Console.ForegroundColor = ConsoleColor.Yellow;

                if (oEventLog == null)
                { return; }

                string textLog = string.Empty;
                if (!string.IsNullOrWhiteSpace(actionInfo))
                { textLog += actionInfo.Trim(); }

                oEventLog.WriteEntry(
                    string.Format(
                        "{0}  Ver_{1}  {2}  {3}",
                        "ActionInfo",
                        BasicAssemblyAttributeAccessors.EntryAssemblyVersion,
                        DateTime.Now.ToString(GlobalizationFormats.ISO_DATETIME_FORMAT_Log),
                        textLog));

                LogAction(textLog, true);
            }
            catch (Exception) { }
        }

        #endregion
    }
}
