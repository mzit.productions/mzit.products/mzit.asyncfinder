﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net;
using System.Web;

namespace MzIT.BasicToolkits.Utility.FetcherHelpers
{
    public static class FetcherServiceResult
    {
        #region Enums Entities

        /// <summary>
        /// Get Special Status Type
        /// </summary>
        public enum FSR_FetcherStatusType : int
        {
            [Description("IsFailed")]
            IsFailed = -1,
            [Description("IsDefined")]
            IsDefined = 0,
            [Description("IsSuccessed")]
            IsSuccessed = 1,

            [Description("IsDeleted")]
            IsDeleted = 2,
            [Description("IsDisabled")]
            IsDisabled = 3,
            [Description("IsInactive")]
            IsInactive = 4,
            [Description("IsCancel")]
            IsCancel = 5,
            [Description("IsSuccessedPlus")]
            IsSuccessedPlus = 6,
            [Description("IsInactivePlus")]
            IsInactivePlus = 7,
            [Description("IsVoid")]
            IsVoid = 8,
        }

        /// <summary>
        /// Get Special Service Type
        /// </summary>
        public enum FSR_FetcherServiceType
        {
            [Description("Exception")]
            Exception = -1,
            [Description("Scaler")]
            Scaler = 1,
            [Description("List OR Collection")]
            List = 2,
            [Description("Multiple")]
            Multiple = 3,
            [Description("Patched")]
            Patched = 4,
        }

        #endregion

        #region Extensions Entities

        public class FSR_FetcherServiceResult
        {
            public string Result { get; set; }
            public object ResultParams { get; set; }
            public string Patched { get; set; }
            public object PatchedParams { get; set; }
            public short Type { get; set; }
            public short Status { get; set; }

            public List<FSR_FetcherServiceError> FetcherServiceErrors { get; set; } = new List<FSR_FetcherServiceError>();
        }

        public class FSR_FetcherServiceError
        {
            public Guid GUID { get; set; } = Guid.NewGuid();
            public int Code { get; set; }
            public string FriendlyMessage { get; set; }
            public string Message { get; set; }
            public Exception Exception { get; set; }
            public FSR_FetcherResponseModel ResponseModel { get; set; }
        }

        public class FSME_FetcherServiceFilter
        {
            public string FriendlyMessage { get; set; } = null;
            public FetcherTryCatch.FTC_PermissionType PermissionType { get; set; } = FetcherTryCatch.FTC_PermissionType.Null;
            public FetcherTryCatch.FTC_PermissionType SecondPermissionType { get; set; } = FetcherTryCatch.FTC_PermissionType.Null;

            public bool? IsThrowException { get; set; } = null;
        }

        #endregion

        #region Extensions Entities - ServerSide

        public class FSR_FetcherResponseModel
        {
            public string Status { get; set; } = FSR_FetcherActionStatus.Unknown.ToString();
            public string UiErrorMessage { get; set; }
            public object Details { get; set; }

            #region GlobalizationDisplayProperties.DisplayProperty_NotMapped

            [NotMapped]
            public string DisplayErrorMessage { get => ("ActionStatus: " + ActionStatus.ToString() + " UiErrorMessage: " + UiErrorMessage); }
            [NotMapped]
            public FSR_FetcherActionStatus ActionStatus { get => EnumExtensions.GetEnumValue<FSR_FetcherActionStatus>(Status); }

            #endregion
        }

        public enum FSR_FetcherActionStatus
        {
            Unknown,

            InvalidEnumValue,
            InvalidModelState,
            InvalidQueryParam,

            FailedToProcess,
            FailedToUpdateDb,
            NotFound,
            InputGuidMismatch,
        }

        #endregion

        #region Methods

        /// <summary>
        /// FetcherServiceResult - Result Mapper
        /// </summary>
        /// <param name="oFetcherResult"></param>
        /// <returns></returns>
        public static T FSR_ResultMapper<T>(FSR_FetcherServiceResult oFetcherResult)
        {
            if (FSR_CheckCondition(oFetcherResult) == FSR_FetcherStatusType.IsFailed)
            { return default(T); }

            try
            {
                if ((T)oFetcherResult.ResultParams != null)
                { return (T)oFetcherResult.ResultParams; }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission); }

            return default(T);
        }

        /// <summary>
        /// FetcherServiceResult - Check Conditions
        /// </summary>
        /// <param name="oFetcherStatus"></param>
        /// <returns>
        /// if (FetcherStatus == null || FetcherStatus.Count == 0) { returnValue = FSR_FetcherStatusType.IsDefined; }
        /// if (IsSuccessed && !IsFailed) { returnValue = FSR_FetcherStatusType.IsSuccessed; }
        /// else if (IsSuccessed && IsFailed) { returnValue = FSR_FetcherStatusType.IsSuccessedPlus; }
        /// else if (!IsSuccessed && IsFailed) { returnValue = FSR_FetcherStatusType.IsFailed; }
        /// else if (!IsSuccessed && !IsFailed) { returnValue = FSR_FetcherStatusType.IsDefined; }</returns>
        public static FSR_FetcherStatusType FSR_CheckConditions(List<FSR_FetcherStatusType> oFetcherStatus)
        {
            FSR_FetcherStatusType returnValue = FSR_FetcherStatusType.IsDefined;

            if (oFetcherStatus == null || oFetcherStatus.Count == 0)
            { return returnValue; }

            try
            {
                var IsSuccessed =
                    (oFetcherStatus
                    .Where(i => i == FSR_FetcherStatusType.IsSuccessed)
                    .Count() > 0);

                var IsFailed =
                    (oFetcherStatus
                    .Where(i => i == FSR_FetcherStatusType.IsFailed)
                    .Count() > 0);

                if (IsSuccessed && !IsFailed)
                { returnValue = FSR_FetcherStatusType.IsSuccessed; }
                else if (IsSuccessed && IsFailed)
                { returnValue = FSR_FetcherStatusType.IsSuccessedPlus; }
                else if (!IsSuccessed && IsFailed)
                { returnValue = FSR_FetcherStatusType.IsFailed; }
                else if (!IsSuccessed && !IsFailed)
                { returnValue = FSR_FetcherStatusType.IsDefined; }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission); }

            return returnValue;
        }

        /// <summary>
        /// FetcherServiceResult - Check Condition
        /// </summary>
        /// <param name="oFetcherResult"></param>
        /// <returns></returns>
        public static FSR_FetcherStatusType FSR_CheckCondition(FSR_FetcherServiceResult oFetcherResult)
        {
            FSR_FetcherStatusType returnValue = FSR_FetcherStatusType.IsFailed;

            if (oFetcherResult == null)
            { return returnValue; }

            try
            {
                returnValue =
                    (FSR_FetcherStatusType)oFetcherResult.Status;
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission); }

            return returnValue;
        }

        /// <summary>
        /// FetcherServiceResult - Append FetcherServiceErrors To FetcherServiceResult
        /// </summary>
        /// <param name="oFetcherResult"></param>
        /// <param name="oFetcherServiceErrors"></param>
        /// <returns></returns>
        public static FSR_FetcherServiceResult FSR_AppendFetcherServiceErrors(FSR_FetcherServiceResult oFetcherResult, List<FSR_FetcherServiceError> oFetcherServiceErrors)
        {
            if (oFetcherServiceErrors?.Count() <= 0)
            { return oFetcherResult; }

            List<Guid> currentGUIDs = null;

            if (oFetcherResult?.FetcherServiceErrors?.Count() > 0)
            {
                currentGUIDs = oFetcherResult?.FetcherServiceErrors?.Select(i => i.GUID).ToList();

                if (oFetcherServiceErrors?.Where(i => currentGUIDs == null || !currentGUIDs.Contains(i.GUID)).Count() <= 0)
                { return oFetcherResult; }
            }

            try
            {
                if (oFetcherResult == null)
                { oFetcherResult = new FSR_FetcherServiceResult(); }

                var serviceErrors = oFetcherServiceErrors?.Where(i => currentGUIDs == null || !currentGUIDs.Contains(i.GUID))?.ToList();
                if (serviceErrors != null)
                { oFetcherResult.FetcherServiceErrors?.AddRange(serviceErrors); }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission); }

            return oFetcherResult;
        }

        /// <summary>
        /// FetcherServiceResult - Find How Many Error By Code
        /// </summary>
        /// <param name="oFetcherResult"></param>
        /// <param name="specialErrorCode"></param>
        /// <returns></returns>
        public static int? FSR_FindHowManyErrorByCode(FSR_FetcherServiceResult oFetcherResult, int specialErrorCode)
        {
            int? returnValue = null;

            if (oFetcherResult == null)
            { return returnValue; }

            try
            {
                returnValue =
                    oFetcherResult?.FetcherServiceErrors?
                    .Where(i => i.Code != 0 ? i.Code == specialErrorCode : (i.ResponseModel != null && (int)i.ResponseModel?.ActionStatus == specialErrorCode))?
                    .Count();
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission); }

            return returnValue;
        }

        /// <summary>
        /// FetcherServiceResult - Find How Many Error By Code
        /// </summary>
        /// <param name="oFetcherResult"></param>
        /// <param name="specialErrorCode"></param>
        /// <returns></returns>
        public static FSR_FetcherServiceError FSR_FindErrorByCode(FSR_FetcherServiceResult oFetcherResult, int specialErrorCode)
        {
            FSR_FetcherServiceError returnValue = null;

            if (oFetcherResult == null)
            { return returnValue; }

            try
            {
                returnValue =
                    oFetcherResult?.FetcherServiceErrors?
                    .Where(i => i.Code != 0 ? i.Code == specialErrorCode : (i.ResponseModel != null && (int)i.ResponseModel?.ActionStatus == specialErrorCode))?
                    .FirstOrDefault();
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission); }

            return returnValue;
        }

        /// <summary>
        /// FetcherServiceResult - Display Friendly Message Error By FetcherServiceError
        /// </summary>
        /// <param name="oFetcherServiceError"></param>
        /// <returns></returns>
        public static string FSR_DisplayFriendlyMessageError(FSR_FetcherServiceError oFetcherServiceError)
        {
            string returnValue = string.Empty;

            if (oFetcherServiceError == null)
            { return returnValue; }

            try
            {
                returnValue =
                    string.IsNullOrWhiteSpace(oFetcherServiceError.FriendlyMessage) ? (oFetcherServiceError.ResponseModel?.UiErrorMessage) : oFetcherServiceError.FriendlyMessage;
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission); }

            return string.IsNullOrWhiteSpace(returnValue) ? string.Empty : returnValue?.Trim();
        }

        /// <summary>
        /// FetcherServiceResult - Display Friendly Message Error By FetcherServiceError
        /// </summary>
        /// <param name="oFetcherServiceError"></param>
        /// <param name="customMessage"></param>
        /// <returns></returns>
        public static string FSR_DisplayFriendlyMessageErrorDeepMode(FSR_FetcherServiceError oFetcherServiceError, string customMessage = null)
        {
            string returnValue = string.Empty;

            if (oFetcherServiceError == null)
            { return returnValue; }

            try
            {
                returnValue =
                    string.IsNullOrWhiteSpace(oFetcherServiceError.ResponseModel?.UiErrorMessage) ? (string.IsNullOrWhiteSpace(customMessage) ? (oFetcherServiceError.FriendlyMessage) : customMessage) : oFetcherServiceError.ResponseModel?.UiErrorMessage;
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission); }

            return string.IsNullOrWhiteSpace(returnValue) ? string.Empty : returnValue?.Trim();
        }

        /// <summary>
        /// FetcherServiceResult - Check Condition
        /// </summary>
        /// <param name="oException"></param>
        /// <returns></returns>
        public static FSR_FetcherResponseModel FSR_SetFetcherResponseModel(Exception oException)
        {
            try
            {
                if (oException as WebException == null)
                { return null; }

                WebException wex = oException as WebException;

                /// Execute Request, catch the exception to eventually get the body
                if (wex.Status == WebExceptionStatus.ProtocolError)
                {
                    string responseBody = HelperFunctions.ThrowResponseBody(wex, false);

                    if (!string.IsNullOrWhiteSpace(responseBody))
                    { return Newtonsoft.Json.JsonConvert.DeserializeObject<FSR_FetcherResponseModel>(responseBody); }
                }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission); }

            return null;
        }

        /// <summary>
        /// FetcherServiceResult - Set FetcherResponseModel By FetcherServiceError
        /// </summary>
        /// <param name="oFetcherError"></param>
        /// <returns></returns>
        public static FSR_FetcherServiceError FSR_SetFetcherResponseModel(FSR_FetcherServiceError oFetcherError)
        {
            try
            {
                if (oFetcherError == null)
                { return oFetcherError; }

                oFetcherError.ResponseModel = FetcherServiceResult.FSR_SetFetcherResponseModel(oFetcherError.Exception);
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission); }

            return oFetcherError;
        }

        /// <summary>
        /// FetcherServiceResult - Set FetcherServiceResult Error
        /// </summary>
        /// <param name="fetcherResult"></param>
        /// <param name="exception"></param>
        /// <param name="filterModel"></param>
        /// <returns></returns>
        public static FSR_FetcherServiceResult FSR_SetFetcherServiceResultError(
            FSR_FetcherServiceResult fetcherResult,
            Exception exception,
            FSME_FetcherServiceFilter filterModel = null)
        {
            if (exception == null)
            { return fetcherResult; }

            if (filterModel == null)
            { filterModel = new FSME_FetcherServiceFilter(); }

            if (filterModel.PermissionType == FetcherTryCatch.FTC_PermissionType.Null)
            { filterModel.PermissionType = FetcherTryCatch.FTC_PermissionType.DataAccessPermission; }

            if (fetcherResult == null)
            { fetcherResult = new FSR_FetcherServiceResult(); }

            fetcherResult.Status = (short)FSR_FetcherStatusType.IsFailed;

            try
            {
                if (exception as HttpException != null)
                {
                    if (filterModel.FriendlyMessage?.Trim() == FetcherTryCatch.FTC_GeneralErrorMessage.ServicesAccessException.ToSpecialDescriptionString())
                    { filterModel.FriendlyMessage = null; }

                    FSR_FetcherServiceError oFetcherServiceError = new FSR_FetcherServiceError
                    {
                        Exception = exception,
                        FriendlyMessage = string.IsNullOrWhiteSpace(filterModel.FriendlyMessage) ? FetcherTryCatch.FTC_GeneralErrorMessage.ServicesAccessHttpException.ToSpecialDescriptionString() : filterModel.FriendlyMessage.Trim(),
                        ResponseModel = FetcherServiceResult.FSR_SetFetcherResponseModel(exception)
                    };

                    fetcherResult.FetcherServiceErrors.Add(oFetcherServiceError);

                    FetcherTryCatch.FTC_GetCEM_Error(oFetcherServiceError, filterModel.PermissionType, filterModel.SecondPermissionType);
                }
                else if (exception as WebException != null)
                {
                    if (filterModel.FriendlyMessage?.Trim() == FetcherTryCatch.FTC_GeneralErrorMessage.ServicesAccessException.ToSpecialDescriptionString())
                    { filterModel.FriendlyMessage = null; }

                    FSR_FetcherServiceError oFetcherServiceError = new FSR_FetcherServiceError
                    {
                        Exception = exception,
                        FriendlyMessage = string.IsNullOrWhiteSpace(filterModel.FriendlyMessage) ? FetcherTryCatch.FTC_GeneralErrorMessage.ServicesAccessWebException.ToSpecialDescriptionString() : filterModel.FriendlyMessage.Trim(),
                        ResponseModel = FetcherServiceResult.FSR_SetFetcherResponseModel(exception)
                    };

                    fetcherResult.FetcherServiceErrors.Add(oFetcherServiceError);

                    FetcherTryCatch.FTC_GetCEM_Error(oFetcherServiceError, filterModel.PermissionType, filterModel.SecondPermissionType);
                }
                else if (exception as Exception != null)
                {
                    FSR_FetcherServiceError oFetcherServiceError = new FSR_FetcherServiceError
                    {
                        Exception = exception,
                        FriendlyMessage = filterModel.FriendlyMessage?.Trim(),
                        ResponseModel = FetcherServiceResult.FSR_SetFetcherResponseModel(exception)
                    };

                    fetcherResult.FetcherServiceErrors.Add(oFetcherServiceError);

                    FetcherTryCatch.FTC_GetCEM_Error(oFetcherServiceError, filterModel.PermissionType, filterModel.SecondPermissionType);
                }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission); }

            if (filterModel?.IsThrowException ?? false)
            { throw new Exception($"Error By FetcherServiceResult: {exception?.Message}"); }

            return fetcherResult;
        }

        #endregion
    }
}
