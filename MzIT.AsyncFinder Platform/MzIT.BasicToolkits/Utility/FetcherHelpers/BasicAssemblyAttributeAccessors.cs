﻿using System.Reflection;

namespace MzIT.BasicToolkits.Utility.FetcherHelpers
{
    public static class BasicAssemblyAttributeAccessors
    {
        #region ExecutingAssemblyAttributeAccessors

        public static string ExecutingAssemblyTitle
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                if (attributes.Length > 0)
                {
                    AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                    if (!string.IsNullOrEmpty(titleAttribute.Title))
                    {
                        return titleAttribute.Title;
                    }
                }

                return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            }
        }

        public static string ExecutingAssemblyVersion => Assembly.GetExecutingAssembly().GetName().Version.ToString();

        public static string ExecutingAssemblyVersionMajor => Assembly.GetExecutingAssembly().GetName().Version.Major.ToString();

        public static string ExecutingAssemblyDescription
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);

                if (attributes.Length == 0)
                    return string.Empty;

                return ((AssemblyDescriptionAttribute)attributes[0]).Description;
            }
        }

        public static string ExecutingAssemblyProduct
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);

                if (attributes.Length == 0)
                    return string.Empty;

                return ((AssemblyProductAttribute)attributes[0]).Product;
            }
        }

        public static string ExecutingAssemblyCopyright
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);

                return (attributes.Length == 0)
                    ? string.Empty
                    : ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }

        public static string ExecutingAssemblyCompany
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);

                return (attributes.Length == 0)
                    ? string.Empty
                    : ((AssemblyCompanyAttribute)attributes[0]).Company;
            }
        }

        #endregion

        #region EntryAssemblyAttributeAccessors

        public static string EntryAssemblyTitle
        {
            get
            {
                object[] attributes = Assembly.GetEntryAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                if (attributes.Length > 0)
                {
                    AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                    if (!string.IsNullOrEmpty(titleAttribute.Title))
                    {
                        return titleAttribute.Title;
                    }
                }
                return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetEntryAssembly().CodeBase);
            }
        }

        public static string EntryAssemblyVersion => Assembly.GetEntryAssembly().GetName().Version.ToString();

        public static string EntryAssemblyVersionMajor => Assembly.GetEntryAssembly().GetName().Version.Major.ToString();

        public static string EntryAssemblyDescription
        {
            get
            {
                object[] attributes = Assembly.GetEntryAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);

                return (attributes.Length == 0)
                    ? string.Empty
                    : ((AssemblyDescriptionAttribute)attributes[0]).Description;
            }
        }

        public static string EntryAssemblyProduct
        {
            get
            {
                object[] attributes = Assembly.GetEntryAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);

                return (attributes.Length == 0)
                    ? string.Empty
                    : ((AssemblyProductAttribute)attributes[0]).Product;
            }
        }

        public static string EntryAssemblyCopyright
        {
            get
            {
                object[] attributes = Assembly.GetEntryAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);

                return (attributes.Length == 0)
                    ? string.Empty
                    : ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }

        public static string EntryAssemblyCompany
        {
            get
            {
                object[] attributes = Assembly.GetEntryAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);

                return (attributes.Length == 0)
                    ? string.Empty
                    : ((AssemblyCompanyAttribute)attributes[0]).Company;
            }
        }

        #endregion
    }
}
