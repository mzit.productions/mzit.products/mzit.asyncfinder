﻿using System;
using System.Reflection;

namespace MzIT.BasicToolkits.Utility.FetcherHelpers
{
    public static class BasicMethods
    {
        #region Special BasicMethods

        /// <summary>
        /// Get or set Basic DefaultPath
        /// </summary>
        private static string _basicDefaultPath =
            string.Format(
                "{0}\\{1}\\{2} {3}",
                Environment.GetFolderPath(Environment.SpecialFolder.Personal),
                BasicAssemblyAttributeAccessors.EntryAssemblyCompany,
                BasicAssemblyAttributeAccessors.EntryAssemblyProduct,
                BasicAssemblyAttributeAccessors.EntryAssemblyVersionMajor);

        /// <summary>
        /// Get or set Basic DefaultPath
        /// </summary>
        public static string BasicDefaultPath
        {
            get => (_basicDefaultPath);
            set { _basicDefaultPath = value; }
        }

        /// <summary>
        /// Get Basic Directory of this application
        /// </summary>
        private static readonly string _baseDirectory =
            System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        //System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);

        /// <summary>
        /// Get Basic Directory of this application
        /// </summary>
        public static string BaseDirectory
        {
            get => (_baseDirectory);
        }

        /// <summary>
        /// Get Basic Directory for extra files
        /// </summary>
        private static readonly string _basicExtraDataDirectory = "Data";

        /// <summary>
        /// Get Basic Directory for extra files
        /// </summary>
        public static string BasicExtraDataDirectory
        {
            get => (_basicExtraDataDirectory);
        }

        /// <summary>
        /// Get Basic Directory for extra articles
        /// </summary>
        private static readonly string _basicExtraArticlesDirectory = "Articles";

        /// <summary>
        /// Get Basic Directory for extra articles
        /// </summary>
        public static string BasicExtraArticlesDirectory
        {
            get => (_basicExtraArticlesDirectory);
        }

        #endregion
    }
}
