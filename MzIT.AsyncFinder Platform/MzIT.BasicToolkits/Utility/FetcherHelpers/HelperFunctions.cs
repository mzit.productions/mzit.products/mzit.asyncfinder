﻿using Force.DeepCloner;
using MzIT.BasicToolkits.UI.Controls.MsgBoxSelectors;
using MzIT.BasicToolkits.Utility.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Media;
using System.Xml;
using System.Xml.Serialization;

namespace MzIT.BasicToolkits.Utility.FetcherHelpers
{
    public static class HelperFunctions
    {
        #region SerializeObject - CloneObject

        private static JsonSerializerSettings GetJsonSerializerSettings()
        {
            return new JsonSerializerSettings
            {
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore,
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
            };
        }

        public static T MapperObject<T>(object source)
        {
            try
            {
                if (source == null) return default(T);

                return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(source, GetJsonSerializerSettings()));
            }
            catch (Exception ex)
            {
                FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission);
                return default(T);
            }
        }

        public static T NewCloneObject<T>(T source)
        {
            try
            {
                if (source == null) return default(T);

                return source.DeepClone();

                //return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(source, GetJsonSerializerSettings()));
            }
            catch (Exception ex)
            {
                FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission);
                return default(T);
            }
        }

        // Clone source object into newClone object
        public static bool CloneObject<T>(this T newClone, T source)
        {
            try
            {
                newClone = NewCloneObject(source);
                return true;
            }
            catch (Exception ex)
            {
                FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission);
                return false;
            }
        }

        /// <summary>
        /// Clone object from source to new clone object
        /// </summary>
        /// <param name="newClone"></param>
        /// <param name="source"></param>
        public static bool DuckCopyShallow(this object newClone, object source)
        {
            try
            {
                if (source == null)
                { newClone = null; }

                var srcT = source.GetType();
                var dstT = newClone.GetType();
                foreach (var f in srcT.GetFields())
                {
                    var dstF = dstT.GetField(f.Name);
                    if (dstF == null)
                        continue;
                    dstF.SetValue(newClone, f.GetValue(source));
                }

                foreach (var f in srcT.GetProperties())
                {
                    var dstF = dstT.GetProperty(f.Name);
                    if (dstF == null)
                        continue;

                    dstF.SetValue(newClone, f.GetValue(source, null), null);
                }

                return true;
            }
            catch (Exception ex)
            {
                FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission);
                return false;
            }
        }

        /// <summary>
        /// Serializes an object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializableObject"></param>
        /// <param name="fileName"></param>
        public static bool SerializeObject<T>(T serializableObject, string fileName, bool appendMode = true)
        {
            if (serializableObject == null)
            { return false; }

            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                XmlSerializer serializer = new XmlSerializer(serializableObject.GetType());
                using (MemoryStream stream = new MemoryStream())
                {
                    serializer.Serialize(stream, serializableObject);
                    stream.Position = 0;
                    xmlDocument.Load(stream);

                    /// Exists File Path
                    CreateDirectoryFullPath(fileName, appendMode);

                    xmlDocument.Save(fileName);
                    stream.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission);
                return false;
            }
        }

        /// <summary>
        /// Deserializes an xml file into an object list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static T DeSerializeObject<T>(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            { return default(T); }

            T objectOut = default(T);

            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(fileName);
                string xmlString = xmlDocument.OuterXml;

                using (StringReader read = new StringReader(xmlString))
                {
                    Type outType = typeof(T);

                    XmlSerializer serializer = new XmlSerializer(outType);
                    using (XmlReader reader = new XmlTextReader(read))
                    {
                        objectOut = (T)serializer.Deserialize(reader);
                        reader.Close();
                    }

                    read.Close();
                }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }

            return objectOut;
        }

        /// <summary>
        /// SerializeObject to json string
        /// JavaScriptSerializer method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializableObject"></param>
        /// <returns></returns>
        public static string Json_SerializeObject<T>(T serializableObject)
        {
            try
            {
                if (serializableObject == null)
                { return string.Empty; }

                //return (new JavaScriptSerializer().Serialize(serializableObject));
                return (Newtonsoft.Json.JsonConvert.SerializeObject(serializableObject));
            }
            catch (Exception ex)
            {
                FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission);
                return string.Empty;
            }
        }

        /// <summary>
        /// Serializes an object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializableObject"></param>
        /// <param name="fileName"></param>
        public static bool Json_SerializeObject_Load<T>(T serializableObject, string fileName)
        {
            if (serializableObject == null)
            { return false; }

            try
            {
                /// Exists File Path
                CreateDirectoryFullPath(fileName);

                Newtonsoft.Json.JsonSerializer serializer = new Newtonsoft.Json.JsonSerializer();
                using (StreamWriter stream = File.CreateText(fileName))
                {
                    serializer.Serialize(stream, serializableObject);

                    stream.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission);
                return false;
            }
        }

        /// <summary>
        /// DeSerializeObject from json string to object
        /// JavaScriptSerializer method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsonStringObject"></param>
        /// <returns></returns>
        public static T Json_DeSerializeObject<T>(string jsonStringObject)
        {
            try
            {
                if (string.IsNullOrEmpty(jsonStringObject))
                { return default(T); }

                JavaScriptSerializer serializer = new JavaScriptSerializer
                {
                    MaxJsonLength = int.MaxValue
                };

                //return (T)new JavaScriptSerializer().Deserialize(jsonStringObject, typeof(T));
                return (T)serializer.Deserialize(jsonStringObject, typeof(T));
            }
            catch (Exception ex)
            {
                FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission);
                return default(T);
            }
        }

        /// <summary>
        /// Deserializes an json file into an object list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static T Json_DeSerializeObject_Load<T>(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            { return default(T); }

            T objectOut = default(T);

            try
            {
                using (StreamReader stream = new StreamReader(fileName))
                {
                    string jsonStringObject = stream.ReadToEnd();

                    objectOut = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(jsonStringObject);

                    stream.Close();
                }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }

            return objectOut;
        }

        /// <summary>
        /// Serializes an object.
        /// BinaryFormatter method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializableObject"></param>
        /// <param name="fileName"></param>
        public static bool BinaryFormatter_SerializeObject<T>(T serializableObject, string fileName)
        {
            if (serializableObject == null)
            { return false; }

            try
            {
                /// Exists File Path
                CreateDirectoryFullPath(fileName);

                using (Stream stream = File.Open(fileName, FileMode.Create))
                {
                    var oBinaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                    oBinaryFormatter.Serialize(stream, serializableObject);
                }

                return true;
            }
            catch (Exception ex)
            {
                FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission);
                return false;
            }
        }

        /// <summary>
        /// DeSerializeObject from fileName string to object
        /// BinaryFormatter method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static T BinaryFormatter_DeSerializeObject<T>(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            { return default(T); }

            T objectOut = default(T);

            try
            {
                using (Stream stream = File.Open(fileName, FileMode.Open))
                {
                    var oBinaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                    objectOut = (T)oBinaryFormatter.Deserialize(stream);

                    stream.Close();
                }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission); }

            return objectOut;
        }

        #endregion

        #region Directory

        /// <summary>
        /// Create Directory Path if not Exists
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string CreateDirectoryPath(string path)
        {
            try
            {
                if (path.Trim() == "")
                {
                    path = BasicMethods.BasicDefaultPath;
                }

                path = path.Trim();

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }

            return path;
        }

        /// <summary>
        /// Create Directory Path plus FileName if not Exists
        /// </summary>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        public static string CreateDirectoryFullPath(string fullPath, bool appendMode = true)
        {
            try
            {
                if (fullPath.Trim() == "")
                {
                    fullPath = BasicMethods.BasicDefaultPath;
                }

                fullPath = fullPath.Trim();

                if (!appendMode || !Directory.Exists(Path.GetDirectoryName(fullPath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(fullPath));
                }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }

            return fullPath;
        }

        /// <summary>
        /// Build new basic full name with special type format
        /// </summary>
        /// <param name="name"></param>
        /// <param name="basic"></param>
        /// <returns></returns>
        public static string BuildFakedFullPathLite(string name, string basic = @"C:\")
        {
            return (
                string.Format(
                    @"{0}{1}",
                    basic,
                    name));
        }

        /// <summary>
        /// Build new basic full name with special type format
        /// </summary>
        /// <param name="name"></param>
        /// <param name="basic"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string BuildFakedFullPath(string name, string basic = @"C:\", string type = "png")
        {
            return (
                string.Format(
                    @"{0}{1}.{2}",
                    basic,
                    name,
                    type));
        }

        /// <summary>
        /// Build new basic full name with special type format
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string BuildFullName(string name, string type)
        {
            return (
                string.Format(
                    "{0}-{1}_{2}_{3}.{4}",
                    BasicAssemblyAttributeAccessors.EntryAssemblyProduct,
                    BasicAssemblyAttributeAccessors.EntryAssemblyVersionMajor,
                    name,
                    DateTime.Now.ToString("yyyy-MM-dd"),
                    type));
        }

        /// <summary>
        /// Build FullPath from folder fullpath and fullname
        /// </summary>
        /// <param name="folderFullPath"></param>
        /// <param name="fullName"></param>
        /// <returns></returns>
        public static string BuildFullPath(string folderFullPath, string fullName)
        {
            return Path.Combine(folderFullPath, fullName);
        }

        #endregion

        #region Application Common

        /// <summary>
        /// System.ComponentModel.DesignerProperties.GetIsInDesignMode(DependencyObject element)
        /// if (HelperFunctions.GetIsInDesignMode(this)) { return; }
        /// if (HelperFunctions.GetIsInDesignMode(new System.Windows.DependencyObject())) { return; }
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static bool GetIsInDesignMode(DependencyObject element)
            => DesignerProperties.GetIsInDesignMode(element);

        public static void ApplicationExit()
        {
            try
            {
                //Application.Current.MainWindow.Close();
                Application.Current.Dispatcher.InvokeShutdown();
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex); }
        }

        /// <summary>
        /// Open a window and close all other in WPF
        /// </summary>
        public static void CloseAllWindows()
        {
            try
            {
                for (int intCounter = Application.Current.Windows.Count - 1; intCounter > 0; intCounter--)
                { Application.Current.Windows[intCounter].Close(); }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex); }
        }

        public static void ApplicationRestart()
        {
            try
            {
                Process.Start(Application.ResourceAssembly.Location);
                Application.Current.Dispatcher.InvokeShutdown();
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex); }
        }

        #endregion

        #region Network and Internet Connection

        [DllImport("wininet.dll")]
        private static extern bool InternetGetConnectedState(out int description, int reservedValue);

        public static bool CheckConnection_WiniNetDll()
        {
            try
            {
                return InternetGetConnectedState(out int description, 0);
            }
            catch (Exception)
            { return false; }
        }

        public static bool CheckConnection_WebClient(string URL = "http://www.google.com")
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead(URL))
                { return true; }
            }
            catch (Exception ex)
            {
                FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.FullPermission);
                return false;
            }
        }

        public static bool CheckConnection_HttpWebRequest(string URL = "http://www.google.com")
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                request.Timeout = 5000;
                request.Credentials = CredentialCache.DefaultNetworkCredentials;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                { return true; }
                else
                { return false; }
            }
            catch (Exception ex)
            {
                FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.FullPermission);
                return false;
            }
        }

        public static bool CheckForInternetConnection(bool IsShowStatus = false, bool IsJustFailure = true, bool IsServerMessageBox = true)
        {
            bool IsConnected = false;

            try
            {
                IsConnected = HelperFunctions.CheckConnection_WiniNetDll();

                if (IsShowStatus)
                {
                    if (!IsJustFailure
                        || (IsJustFailure && !IsConnected))
                    {
                        if (IsConnected)
                        {
                            MsgBoxSelector.ShowDialog(
                                LEE_MetroMsgBoxSelectorType.Success,
                                "Great! You are connected to the internet ...",
                                "Internet Connection");
                        }
                        else
                        {
                            if (IsServerMessageBox)
                            {
                                MsgBoxSelector.ShowDialog(
                                    LEE_MetroMsgBoxSelectorType.Error,
                                    "Unable to connect to server. Please check your internet connection and try again ...",
                                    "Internet Connection");
                            }
                            else
                            {
                                MsgBoxSelector.ShowDialog(
                                    LEE_MetroMsgBoxSelectorType.Error,
                                    "Unable to connect. Please check your internet connection and try again ...",
                                    "Internet Connection");
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.FullPermission); }

            return IsConnected;
        }

        public static GME_InternetConnectionStatus CheckForInternetConnection(GME_InternetConnectionStatus oInternetConnectionStatus, bool IsShowOfflineError = false)
        {
            try
            {
                if (oInternetConnectionStatus == null)
                { oInternetConnectionStatus = new GME_InternetConnectionStatus(); }

                bool isInProgress = true;

                while (isInProgress)
                {
                    bool connectFlag = true;

                    /// Check internet connection
                    oInternetConnectionStatus.Network_IsConnected = HelperFunctions.CheckForInternetConnection();
                    if (!oInternetConnectionStatus.Network_IsConnected)
                    {
                        connectFlag = false;
                        if (oInternetConnectionStatus.Network_TryCatchConnectingAttemptCounter > oInternetConnectionStatus.Network_TryCatchConnectingAttempt)
                        {
                            isInProgress = false;
                            oInternetConnectionStatus.Network_TryCatchConnectingAttemptCounter = 0;

                            if (IsShowOfflineError)
                            {
                                MsgBoxSelector.ShowDialog(
                                    LEE_MetroMsgBoxSelectorType.Error,
                                    "Unable to connect to server. Please check your internet connection and try again ...",
                                    "Internet Connection");
                            }
                        }
                        else
                        {
                            oInternetConnectionStatus.Network_TryCatchConnectingAttemptCounter++;
                        }
                    }

                    if (connectFlag)
                    {
                        isInProgress = false;
                    }
                }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.FullPermission); }

            return oInternetConnectionStatus;
        }

        public static string GetLocalIPAddress()
        {
            try
            {
                var host = Dns.GetHostEntry(Dns.GetHostName());
                foreach (var ip in host.AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        return ip.ToString();
                    }
                }
                //throw new Exception("No network adapters with an IPv4 address in the system!");
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }

            return "127.0.0.1";
        }

        /// <summary>
        /// Tries to rethrow the WebException with the data from the body included, if possible.
        /// Otherwise just rethrows the original message.
        /// </summary>
        /// <param name="wex">The web exception.</param>
        /// <exception cref="WebException"></exception>
        /// <remarks>
        /// By default, on protocol errors, the body is not included in web exceptions.
        /// This solutions includes potentially relevant information for resolving the issue.
        /// </remarks>
        public static void ThrowWithBody(WebException wex)
        {
            if (wex.Status == WebExceptionStatus.ProtocolError)
            {
                string responseBody;
                try
                {
                    /// Get the message body for rethrow with body included
                    responseBody = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd();
                }
                catch (Exception)
                {
                    /// In case of failure to get the body just rethrow the original web exception.
                    throw wex;
                }

                /// Include the body in the message
                throw new WebException(wex.Message + $" Response body: '{responseBody}'", wex, wex.Status, wex.Response);
            }

            /// In case of non-protocol errors no body is available anyway, so just rethrow the original web exception.
            throw wex;
        }

        /// <summary>
        /// Tries to rethrow the WebException with the data from the body included, if possible.
        /// Otherwise just rethrows the original message.
        /// </summary>
        /// <param name="wex">The web exception.</param>
        /// <param name="IsThrowException"></param>
        /// <exception cref="WebException"></exception>
        /// <remarks>
        /// By default, on protocol errors, the body is not included in web exceptions.
        /// This solutions includes potentially relevant information for resolving the issue.
        /// </remarks>
        public static string ThrowResponseBody(
            WebException wex,
            bool IsThrowException = false)
        {
            if (wex.Status == WebExceptionStatus.ProtocolError)
            {
                string responseBody = string.Empty;

                try
                {
                    /// Get the message body for rethrow with body included
                    responseBody = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd();
                }
                catch (Exception)
                {
                    /// In case of failure to get the body just rethrow the original web exception.
                    if (IsThrowException)
                    { throw wex; }
                }

                /// Include the body in the message
                //throw new WebException(wex.Message + $" Response body: '{responseBody}'", wex, wex.Status, wex.Response);
                return responseBody;
            }

            /// In case of non-protocol errors no body is available anyway, so just rethrow the original web exception.
            if (IsThrowException)
            { throw wex; }

            return string.Empty;
        }

        #endregion

        #region Methods

        /// <summary>
        /// IsEnable Function
        /// Force to run or other reason
        /// </summary>
        /// <returns></returns>
        public static bool IsEnableFunction()
        {
            return true;
        }

        /// <summary>
        /// IsDisable Function
        /// For the future or other reason
        /// </summary>
        /// <returns></returns>
        public static bool IsDisableFunction()
        {
            return false;
        }

        public static string Converter_ToTrimContent(string value, List<string> oldChars = null, string newChar = " ", bool isNotNull = false, string forceValue = null, bool isTrimEnd = false)
        {
            try
            {
                value = (value == null ? string.Empty : value.Trim());

                if (!string.IsNullOrEmpty(value) && oldChars != null && oldChars.Count > 0)
                {
                    if (isTrimEnd)
                    { oldChars.ForEach(i => value = value.TrimEnd(i.ToCharArray()[0]).Trim()); }
                    else
                    { oldChars.ForEach(i => value = value.Replace(i, newChar).Trim()); }
                }

                if (isNotNull && string.IsNullOrWhiteSpace(value))
                {
                    if (string.IsNullOrWhiteSpace(forceValue))
                    { forceValue = GlobalizationFormats.DisableParameter; }

                    value = forceValue;
                }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission); }

            return value;
        }

        public static int GetRandomMode(int minValue, int maxValue)
        {
            try
            {
                Random oRandom = new Random();
                return oRandom.Next(minValue, maxValue);
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }

            return -1;
        }

        public static SolidColorBrush StringToSolidColorBrush(string hashColor)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(hashColor) && hashColor.Trim().Length > 0)
                { return (SolidColorBrush)(new BrushConverter().ConvertFromString(hashColor)); }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }

            return Brushes.Transparent;
        }

        #endregion
    }
}
