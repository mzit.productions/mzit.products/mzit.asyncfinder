﻿using MzIT.BasicToolkits.UI.Controls.MsgBoxSelectors;
using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using static MzIT.BasicToolkits.Utility.FetcherHelpers.FetcherServiceResult;

namespace MzIT.BasicToolkits.Utility.FetcherHelpers
{
    public static class FetcherTryCatch
    {
        public static void FTC_GetCEM_Error(
            Exception exception,
            FTC_PermissionType permissionType = FTC_PermissionType.Null,
            FTC_PermissionType secondPermissionType = FTC_PermissionType.Null,
            string friendlyMessage = null)
        {
            FTC_CatchExceptionMessageError(
                (!string.IsNullOrWhiteSpace(friendlyMessage)) ? (new FSR_FetcherServiceError { FriendlyMessage = friendlyMessage }) : null,
                exception,
                permissionType,
                secondPermissionType);
        }

        public static void FTC_GetCEM_Error(
            FSR_FetcherServiceError FSR_FetcherServiceError,
            FTC_PermissionType permissionType = FTC_PermissionType.Null,
            FTC_PermissionType secondPermissionType = FTC_PermissionType.Null)
        {
            if (FSR_FetcherServiceError == null) return;

            FTC_CatchExceptionMessageError(FSR_FetcherServiceError, FSR_FetcherServiceError.Exception, permissionType, secondPermissionType);
        }

        private static void FTC_CatchExceptionMessageError(
            FSR_FetcherServiceError FSR_FetcherServiceError,
            Exception exception,
            FTC_PermissionType permissionType,
            FTC_PermissionType secondPermissionType)
        {
            if (exception == null) return;

            try
            {
                if (permissionType != FTC_PermissionType.DoNotLogError || secondPermissionType != FTC_PermissionType.DoNotLogError)
                { Logging.LogError(exception, FSR_FetcherServiceError?.FriendlyMessage, FSR_FetcherServiceError?.ResponseModel?.DisplayErrorMessage); }

                bool mustDisplayErrorMessage = permissionType == FTC_PermissionType.MustDisplayErrorMessage || secondPermissionType == FTC_PermissionType.MustDisplayErrorMessage;

                if (mustDisplayErrorMessage || (ShouldDisplayErrorMessage(permissionType) && ShouldDisplayErrorMessage(secondPermissionType)))
                {
                    string friendlyErrorMessage = exception.Message;

                    if (!string.IsNullOrWhiteSpace(FSR_FetcherServiceError?.ResponseModel?.UiErrorMessage))
                        friendlyErrorMessage = FSR_FetcherServiceError?.ResponseModel?.UiErrorMessage;
                    else if (!string.IsNullOrWhiteSpace(FSR_FetcherServiceError?.FriendlyMessage))
                        friendlyErrorMessage = FSR_FetcherServiceError?.FriendlyMessage;

                    if (exception is ThreadAbortException)
                    { friendlyErrorMessage = string.Empty; }

                    if (!string.IsNullOrWhiteSpace(friendlyErrorMessage))
                    { MsgBoxSelector.ShowDialog(LEE_MetroMsgBoxSelectorType.TryCatchError, friendlyErrorMessage, "Error: "); }

                    /// This should be only shown in development mode
                    if (TryCatchDisplayErrorMessagePermission.TryCatchIsShowNotFriendlyMessageError)
                    { MsgBoxSelector.ShowDialog(LEE_MetroMsgBoxSelectorType.TryCatchError, JsonConvert.SerializeObject(exception), "Exception: "); }
                }
            }
            catch (Exception ex) { Debug.WriteLine($"Error in FTC_CatchExceptionMessageError: {JsonConvert.SerializeObject(ex)}"); }
        }

        private static bool ShouldDisplayErrorMessage(FTC_PermissionType permissionType)
        {
            if (!TryCatchDisplayErrorMessagePermission.TryCatchIsShowMessageError) return false;

            switch (permissionType)
            {
                case FTC_PermissionType.Null:
                    return true;
                case FTC_PermissionType.FullPermission:
                    return TryCatchDisplayErrorMessagePermission.TryCatchIsAllowFullPermissionShowMessageError;
                case FTC_PermissionType.BasicModelsPermission:
                    return TryCatchDisplayErrorMessagePermission.TryCatchIsAllowBasicModelsPermissionShowMessageError;
                case FTC_PermissionType.DataAccessPermission:
                    return TryCatchDisplayErrorMessagePermission.TryCatchIsAllowDataAccessPermissionShowMessageError;
                case FTC_PermissionType.ServicesAccessPermission:
                    return TryCatchDisplayErrorMessagePermission.TryCatchIsAllowServicesAccessPermissionShowMessageError;
                default:
                    return false; /// This should never happen
            }
        }

        public enum FTC_GeneralErrorMessage
        {
            [SpecialDescription("An unexpected error!")]
            UnexpectedError,

            [SpecialDescription("Complete the following sections:\n")]
            RegistrationProcessValidationError,
            [SpecialDescription("An error occurred during your registration process!")]
            RegistrationProcessPrimaryError,
            [SpecialDescription("An error occurred while trying to edit registered information!")]
            RegistrationProcessEditError,
            [SpecialDescription("An error occurred while trying to delete registered information!")]
            RegistrationProcessDeleteError,

            [SpecialDescription("Connection Error Type 1: There is something wrong with your Internet connection!")]
            ServicesAccessHttpException,
            [SpecialDescription("Oops something went wrong!")]
            ServicesAccessWebException,
            [SpecialDescription("Error Type 1: Error Please Call Support Group!")]
            ServicesAccessException
        }

        public enum FTC_PermissionType
        {
            Null = 0,
            DoNotLogError,
            MustDisplayErrorMessage,

            FullPermission,
            BasicModelsPermission,
            DataAccessPermission,
            ServicesAccessPermission,
        }

        private class TryCatchDisplayErrorMessagePermission
        {
            public static bool TryCatchIsShowMessageError { get; set; } = true;
            public static bool TryCatchIsShowNotFriendlyMessageError { get; set; } = true;

            public static bool TryCatchIsAllowFullPermissionShowMessageError { get; set; } = false;
            public static bool TryCatchIsAllowBasicModelsPermissionShowMessageError { get; set; } = true;
            public static bool TryCatchIsAllowDataAccessPermissionShowMessageError { get; set; } = true;
            public static bool TryCatchIsAllowServicesAccessPermissionShowMessageError { get; set; } = true;
        }
    }
}
