﻿using MzIT.BasicToolkits.Utility.FetcherHelpers;
using System;

namespace MzIT.BasicToolkits.Utility.MultiThreadedUtility
{
    public class MultiThreadedPeriodicFunctions_TEMPLATE
    {
        #region MultiThreadedPeriodicFunctions - TEMPLATE

        #region MultiThreadedPeriodicFunctions Fields

        /// <summary>
        /// In app, this is a member variable of the main window
        /// </summary>
        internal MultiThreadedPeriodicFunctions MTPF_TEMPLATE { get; set; }

        #endregion

        #region MultiThreadedPeriodicFunctions Methods

        /// <summary>
        /// Set MultiThreadedPeriodicFunctions Details for this application
        /// </summary>
        /// <param name="IsAutoActive"></param>
        internal void SetMultiThreadedPeriodicFunctionsDetails_TEMPLATE(bool IsAutoActive)
        {
            try
            {
                if (MTPF_TEMPLATE == null)
                {
                    MTPF_TEMPLATE = new MultiThreadedPeriodicFunctions
                    {
                        NextAuthorizationThreshold = new NextAuthorizationThreshold(LEE_NextAuthorizationThresholdType.PeriodicFunctions_Normal)
                        {
                            //TimePassedThreshold = App.SpecialThresholdRefreshTEMPLATE
                        }
                    };

                    MTPF_TEMPLATE.OnActive_Tick += new System.Timers.ElapsedEventHandler(MTPF_DispatcherTimerProcess_OnActive_Tick_TEMPLATE);
                }

                if (IsAutoActive)
                { MTPF_TEMPLATE.MultiThreadedPeriodicFunctions_ActiveMTPF(true); }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex); }
        }

        /// <summary>
        /// Do something works in a period in special thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MTPF_DispatcherTimerProcess_OnActive_Tick_TEMPLATE(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                FetcherMultiThreadedUtility.MTF_StartThread_Invoke(MTPF_TimerCounterProcess_OnActive_TEMPLATE);
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex); }
        }

        /// <summary>
        /// Counter of this timer
        /// Check state in special period and do something if NextAuthorizationThreshold.IsEnabledCore==True
        /// Update PeriodicFunctions from server in special thread separate of main application
        /// </summary>
        private void MTPF_TimerCounterProcess_OnActive_TEMPLATE()
        {
            try
            {
                if (MTPF_TEMPLATE.IsDisabledRunFunction())
                { return; }

                MTPF_RunPeriodicFunctions_TEMPLATE(true);
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.FullPermission, FetcherTryCatch.FTC_PermissionType.DoNotLogError); }
        }

        /// <summary>
        /// Refresh TEMPLATE
        /// Check Network Connection if Refresh DataSource
        /// </summary>
        /// <param name="IsRefreshDataSource"></param>
        /// <returns></returns>
        internal bool MTPF_RunPeriodicFunctions_TEMPLATE(bool IsRefreshDataSource)
        {
            bool IsValid = true;

            try
            {
                if (MTPF_TEMPLATE.BeginRunFunction(IsRefreshDataSource))
                {
                    /// GlobalizationDisplayProperties.DisplayProperty_DoSomething;

                    MTPF_TEMPLATE.EndRunFunction();
                }
                else { MTPF_TEMPLATE.EndRunFunction(); }
            }
            catch (Exception ex)
            {
                IsValid = false;
                MTPF_TEMPLATE.EndRunFunction();
                FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.FullPermission);
            }

            return IsValid;
        }

        #endregion

        #endregion
    }
}
