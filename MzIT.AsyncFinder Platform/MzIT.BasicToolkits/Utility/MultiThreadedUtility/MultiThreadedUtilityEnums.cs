﻿
/// <!-- LEE_NextAuthorizationThresholdType 1.0.0.0 -->

namespace MzIT.BasicToolkits.Utility.MultiThreadedUtility
{
    public enum LEE_NextAuthorizationThresholdType
    {
        IsDefined = 0,
        /// <summary>
        /// Button
        /// </summary>
        Click,
        Report,
        PeriodicFunctions_Short,
        PeriodicFunctions_Normal,
        PeriodicFunctions_Long,
        PeriodicFunctions_ExtraLong,
    }
}
