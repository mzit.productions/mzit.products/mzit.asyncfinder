﻿using MzIT.BasicToolkits.Utility.FetcherHelpers;
using System;

/// <!-- NextAuthorizationThreshold 1.0.0.0 -->

namespace MzIT.BasicToolkits.Utility.MultiThreadedUtility
{
    public class NextAuthorizationThreshold
    {
        #region Static Fields

        public static long TimePassedThreshold_IsDefined { get; set; } = (30 * 1000);
        public static long TimePassedThreshold_Click { get; set; } = (2 * 1000);
        public static long TimePassedThreshold_Report { get; set; } = (24 * 60 * 60 * 1000);
        public static long TimePassedThreshold_PeriodicFunctions_Short { get; set; } = (5 * 1000);
        public static long TimePassedThreshold_PeriodicFunctions_Normal { get; set; } = (20 * 1000);
        public static long TimePassedThreshold_PeriodicFunctions_Long { get; set; } = (2 * 60 * 1000);
        public static long TimePassedThreshold_PeriodicFunctions_ExtraLong { get; set; } = (15 * 60 * 1000);

        #endregion

        #region Constructor

        public NextAuthorizationThreshold()
        {

        }

        public NextAuthorizationThreshold(LEE_NextAuthorizationThresholdType typeMode) : this()
        {
            IsSetDefaultProperties = true;
            TypeMode = typeMode;
        }

        #endregion

        #region Private Fields

        /// <summary>
        /// TimePassed NextAuthorizationThresholdPermission datetime Threshold
        /// </summary>
        private DateTime? NextAuthorizationThresholdRunFunction { get; set; } = null;

        #endregion

        #region Public Fields

        public bool IsSetDefaultProperties { get; set; } = true;

        private LEE_NextAuthorizationThresholdType _TypeMode { get; set; } = LEE_NextAuthorizationThresholdType.IsDefined;
        /// <summary>
        /// Get Special Type Mode By Next Authorization Threshold
        /// </summary>
        internal LEE_NextAuthorizationThresholdType TypeMode
        {
            get { return _TypeMode; }
            set
            {
                _TypeMode = value;
                OnTypeModeChanged(_TypeMode);
            }
        }

        /// <summary>
        /// Time in milliseconds to fire the OnTimePassed event.
        /// Plain timer, for example going off every 30 secs
        /// Default is TimeSpan.FromMilliseconds(30 * 1000);
        /// </summary>
        public long? TimePassedThreshold { get; set; } = null;

        /// <summary>
        /// Time in milliseconds to fire the OnTimePassed event.
        /// Plain timer, for example going off every 30 secs
        /// Default is TimeSpan.FromMilliseconds(1 * 1000);
        /// </summary>
        public long? TimePassedIntervalThreshold { get; set; } = null;

        /// <summary>
        /// Block RunFunction
        /// </summary>
        public bool IsBlock { get; set; } = false;
        /// <summary>
        /// Activated RunFunction
        /// </summary>
        public bool? IsActivated { get; set; } = null;
        /// <summary>
        /// Force refresh method!
        /// </summary>
        public bool IsForce { get; set; } = false;

        /// <summary>
        /// No allowed when properties and fields refresh in another method!
        /// </summary>
        public bool IsRefreshDataSource { get; set; } = false;
        /// <summary>
        /// No allowed when properties and fields use in another method!
        /// </summary>
        public bool IsUsedPropertiesDetails { get; set; } = false;

        /// <summary>
        /// No allowed when properties and fields refresh in another method!
        /// </summary>
        public bool IsResetDataSource { get; set; } = false;

        #endregion

        #region GlobalizationDisplayProperties.DisplayProperty_NotMapped

        /// <summary>
        /// Is Next RunFunction limited by TimePassed Authorization datetime Threshold
        /// </summary>
        public bool IsEnabledCore { get { return IsNextAuthorizationThresholdPermission(); } }

        #endregion

        #region Private Methods

        private void OnTypeModeChanged(LEE_NextAuthorizationThresholdType typeMode)
        {
            try
            {
                if (IsSetDefaultProperties)
                {
                    if (TimePassedIntervalThreshold == null)
                    { TimePassedIntervalThreshold = 1 * 1000; }

                    switch (typeMode)
                    {
                        case LEE_NextAuthorizationThresholdType.IsDefined:
                        default:
                            {
                                if (TimePassedThreshold == null)
                                { TimePassedThreshold = NextAuthorizationThreshold.TimePassedThreshold_IsDefined; }

                                break;
                            }
                        case LEE_NextAuthorizationThresholdType.Click:
                            {
                                if (TimePassedThreshold == null)
                                { TimePassedThreshold = NextAuthorizationThreshold.TimePassedThreshold_Click; }

                                break;
                            }
                        case LEE_NextAuthorizationThresholdType.Report:
                            {
                                if (TimePassedThreshold == null)
                                { TimePassedThreshold = NextAuthorizationThreshold.TimePassedThreshold_Report; }

                                if (IsActivated == null)
                                { IsActivated = false; }

                                break;
                            }
                        case LEE_NextAuthorizationThresholdType.PeriodicFunctions_Short:
                            {
                                if (TimePassedThreshold == null)
                                { TimePassedThreshold = NextAuthorizationThreshold.TimePassedThreshold_PeriodicFunctions_Short; }

                                break;
                            }
                        case LEE_NextAuthorizationThresholdType.PeriodicFunctions_Normal:
                            {
                                if (TimePassedThreshold == null)
                                { TimePassedThreshold = NextAuthorizationThreshold.TimePassedThreshold_PeriodicFunctions_Normal; }

                                break;
                            }
                        case LEE_NextAuthorizationThresholdType.PeriodicFunctions_Long:
                            {
                                if (TimePassedThreshold == null)
                                { TimePassedThreshold = NextAuthorizationThreshold.TimePassedThreshold_PeriodicFunctions_Long; }

                                break;
                            }
                        case LEE_NextAuthorizationThresholdType.PeriodicFunctions_ExtraLong:
                            {
                                if (TimePassedThreshold == null)
                                { TimePassedThreshold = NextAuthorizationThreshold.TimePassedThreshold_PeriodicFunctions_ExtraLong; }

                                break;
                            }
                    }
                }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.FullPermission); }
        }

        /// <summary>
        /// Is Next RunFunction limited by TimePassed Authorization datetime Threshold
        /// Also if IsUsedPropertiesDetails == False
        /// Also if IsRefreshDataSource == False
        /// </summary>
        /// <returns></returns>
        private bool IsNextAuthorizationThresholdPermission()
        {
            try
            {
                if (IsBlock)
                { return false; }

                if ((NextAuthorizationThresholdRunFunction != null && DateTime.UtcNow < NextAuthorizationThresholdRunFunction)
                    || IsUsedPropertiesDetails
                    || IsRefreshDataSource)
                {
                    if (!IsForce)
                    { return false; }
                }
                if (!(IsActivated ?? true))
                { return false; }

                IsForce = false;

                NextAuthorizationThresholdRunFunction =
                    DateTime.UtcNow +
                    TimeSpan.FromMilliseconds(TimePassedThreshold ?? (30 * 1000));
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.FullPermission); }

            return true;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Reset Next Authorization Threshold RunFunction
        /// Set Default values for RunFunction Fields and functions of this timer
        /// </summary>
        public void Reset()
        {
            NextAuthorizationThresholdRunFunction = null;
        }

        /// <summary>
        /// Reset Next Authorization Threshold RunFunction
        /// Set Default values for RunFunction Fields and functions of this timer
        /// Plus Reset IsForce
        /// </summary>
        public void ResetAndActive()
        {
            IsActivated = true;
            NextAuthorizationThresholdRunFunction = null;
        }

        /// <summary>
        /// Reset Next Authorization Threshold RunFunction
        /// Set Default values for RunFunction Fields and functions of this timer
        /// Plus Reset IsForce
        /// </summary>
        public void ResetPro()
        {
            IsForce = false;
            IsActivated = true;
            IsUsedPropertiesDetails = false;
            NextAuthorizationThresholdRunFunction = null;
        }

        #endregion
    }
}
