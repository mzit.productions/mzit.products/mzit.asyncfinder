﻿using MzIT.BasicToolkits.Utility.Entities;
using MzIT.BasicToolkits.Utility.FetcherHelpers;
using System;
using System.Threading;

/// <!-- MultiThreadedPeriodicFunctions 1.0.0.0 -->

namespace MzIT.BasicToolkits.Utility.MultiThreadedUtility
{
    public class MultiThreadedPeriodicFunctions
    {
        #region Constructor

        public MultiThreadedPeriodicFunctions()
        {

        }

        #endregion

        #region Events

        /// <summary>
        /// DispatcherTimerProcess_Tick
        /// </summary>
        public event System.Timers.ElapsedEventHandler OnActive_Tick;

        #endregion

        #region Static Fields

        public static GME_InternetConnectionStatus InternetConnectionStatus { get; set; } = new GME_InternetConnectionStatus();

        #endregion

        #region Fields

        /// <summary>
        /// Get/Set status of timer
        /// If Reset is True, then go to default value for Fields and methods this timer
        /// </summary>
        private bool IsResetStatusProcess { get; set; } = false;

        /// <summary>
        /// Main DispatcherTimer for this MultiThreaded Functions to check stopwatch time in special tick
        /// </summary>
        private System.Timers.Timer DispatcherTimerProcess { get; set; } = new System.Timers.Timer();

        /// <summary>
        /// Main thread for refresh RunFunction
        /// </summary>
        public Thread ThreadTimerProcess { get; set; }
        /// <summary>
        /// Main thread start for refresh RunFunction
        /// </summary>
        public ThreadStart ThreadStartTimerProcess { get; set; }

        #endregion

        #region GlobalizationDisplayProperties.DisplayProperty_NotMapped

        public string DisplayStatusProcess { get { return DispatcherTimerProcess.Enabled ? "Started" : "Stoped"; } }
        public string DisplayStatusTimer { get { return DispatcherTimerProcess.Enabled ? "Lap" : "Reset"; } }

        #endregion

        #region Common Params

        /// <summary>
        /// Get some params for filter in view!
        /// No get again from server!
        /// </summary>
        public object MainFilters { get; set; }
        /// <summary>
        /// Get some params for filter in view!
        /// No get again from server!
        /// </summary>
        public object SecondaryFilters { get; set; }
        /// <summary>
        /// Need to get this objects from server and then set to Dependency Propertie of another thread in Dispatcher.Invoke state
        /// </summary>
        public object ResultParams { get; set; }
        /// <summary>
        /// Need to get this objects from server and then set to Dependency Propertie of another thread in Dispatcher.Invoke state
        /// </summary>
        public object SecondaryResultParams { get; set; }

        #endregion

        #region NextAuthorizationThreshold

        private NextAuthorizationThreshold _NextAuthorizationThreshold { get; set; }
        public NextAuthorizationThreshold NextAuthorizationThreshold
        {
            get
            {
                if (_NextAuthorizationThreshold == null)
                { _NextAuthorizationThreshold = new NextAuthorizationThreshold(LEE_NextAuthorizationThresholdType.PeriodicFunctions_Normal); }
                return _NextAuthorizationThreshold;
            }
            set { _NextAuthorizationThreshold = value; }
        }

        #endregion

        #region Status

        /// <summary>
        /// Set Default values for PeriodicFunctions Fields and functions of this  timer 
        /// </summary>
        private void TimerResetProcess()
        {
            try
            {
                NextAuthorizationThreshold.Reset();
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }
        }

        /// <summary>
        /// Start this timer
        /// </summary>
        private void TimerStartProcess()
        {
            try
            {
                if (IsResetStatusProcess)
                {
                    TimerResetProcess();
                    IsResetStatusProcess = false;
                }

                DispatcherTimerProcess.Enabled = true;
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }
        }

        /// <summary>
        /// Lap this timer and get current status
        /// </summary>
        private void TimerLapProcess()
        {

        }

        /// <summary>
        /// Stop this timer
        /// </summary>
        private void TimerStopProcess()
        {
            try
            {
                DispatcherTimerProcess.Enabled = false;
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }
        }

        /// <summary>
        /// Reset/Lap PeriodicFunctions
        /// </summary>
        private void ResetLapProcess()
        {
            try
            {
                if (DispatcherTimerProcess.Enabled)
                { TimerLapProcess(); }
                else
                { TimerResetProcess(); }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }
        }

        #endregion

        #region Functions

        /// <summary>
        /// Change enabled if timer and set special status for each state
        /// </summary>
        /// <param name="IsEnabledTimer"></param>
        /// <returns></returns>
        private bool DispatcherTimerProcess_EnabledChanged(bool IsEnabledTimer)
        {
            return true;
        }

        /// <summary>
        /// Change Start/Stop PeriodicFunctions
        /// </summary>
        private void DispatcherTimerStartStopProcess(bool IsForceChange, bool IsTimerEnabled)
        {
            try
            {
                if (IsForceChange)
                {
                    if (IsTimerEnabled)
                    {
                        TimerStartProcess();

                        DispatcherTimerProcess_EnabledChanged(IsTimerEnabled);
                        TimerLapProcess();
                    }
                    else
                    {
                        TimerStopProcess();

                        DispatcherTimerProcess_EnabledChanged(IsTimerEnabled);
                        TimerLapProcess();
                    }
                }
                else
                {
                    if (!DispatcherTimerProcess.Enabled)
                    {
                        TimerStartProcess();

                        DispatcherTimerProcess_EnabledChanged(DispatcherTimerProcess.Enabled);
                        TimerLapProcess();
                    }
                    else
                    {
                        TimerStopProcess();

                        DispatcherTimerProcess_EnabledChanged(DispatcherTimerProcess.Enabled);
                        TimerLapProcess();
                    }
                }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Refresh InternetConnectionStatus items source
        /// </summary>
        /// <param name="IsShowOfflineError"></param>
        /// <returns></returns>
        private static bool RefreshInternetConnectionStatus(bool IsShowOfflineError = false)
        {
            MultiThreadedPeriodicFunctions.InternetConnectionStatus =
                HelperFunctions.CheckForInternetConnection(MultiThreadedPeriodicFunctions.InternetConnectionStatus, (MultiThreadedPeriodicFunctions.InternetConnectionStatus.Network_TryCatchIsShowOfflineError || IsShowOfflineError));

            return MultiThreadedPeriodicFunctions.InternetConnectionStatus.Network_IsConnected;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Set Details of MultiThreaded RunFunction
        /// </summary>
        /// <param name="IsForceStart"></param>
        public void MultiThreadedPeriodicFunctions_ActiveMTPF(bool IsForceStart)
        {
            try
            {
                DispatcherTimerProcess.Interval =
                    TimeSpan.FromMilliseconds(NextAuthorizationThreshold.TimePassedIntervalThreshold ?? (1 * 1000)).TotalMilliseconds;

                DispatcherTimerProcess.Elapsed += OnActive_Tick;

                DispatcherTimerProcess_EnabledChanged(DispatcherTimerProcess.Enabled);
                IsResetStatusProcess = true;

                if (IsForceStart)
                { DispatcherTimerStartStopProcess(true, true); }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }
        }

        public bool? IsActiveTimer()
        {
            return DispatcherTimerProcess?.Enabled;
        }

        public bool IsEnabledRunFunction()
        {
            return (NextAuthorizationThreshold.IsEnabledCore);
        }

        public bool IsDisabledRunFunction()
        {
            return (!NextAuthorizationThreshold.IsEnabledCore);
        }

        /// <summary>
        /// Begin RunFunction
        /// </summary>
        /// <param name="IsRefreshDataSource">Default is True;</param>
        /// <param name="IsCheckInternetConnected">Default is True;</param>
        /// <param name="IsShowOfflineError">Default is False;</param>
        /// <returns></returns>
        private bool BeginRunFunctionCore(bool IsRefreshDataSource = true, bool IsCheckInternetConnected = true, bool IsShowOfflineError = false)
        {
            if (IsRefreshDataSource)
            {
                NextAuthorizationThreshold.IsRefreshDataSource = true;

                if (IsCheckInternetConnected)
                { return (MultiThreadedPeriodicFunctions.RefreshInternetConnectionStatus(IsShowOfflineError)); }
            }

            return true;
        }

        /// <summary>
        /// Begin RunFunction
        /// </summary>
        /// <param name="IsRefreshDataSource">Default is True;</param>
        /// <param name="IsCheckInternetConnected">Default is True;</param>
        /// <returns></returns>
        public bool BeginRunFunction(bool IsRefreshDataSource)
            => BeginRunFunctionCore(IsRefreshDataSource);

        /// <summary>
        /// Begin RunFunction
        /// </summary>
        /// <param name="IsRefreshDataSource">Default is True;</param>
        /// <param name="IsCheckInternetConnected">Default is True;</param>
        /// <returns></returns>
        public bool BeginRunFunction(bool IsRefreshDataSource, bool IsCheckInternetConnected)
            => BeginRunFunctionCore(IsRefreshDataSource, IsCheckInternetConnected);

        /// <summary>
        /// Begin RunFunction
        /// </summary>
        /// <param name="IsRefreshDataSource">Default is True;</param>
        /// <param name="IsCheckInternetConnected">Default is True;</param>
        /// <param name="IsShowOfflineError">Default is False;</param>
        /// <returns></returns>
        public bool BeginRunFunction(bool IsRefreshDataSource, bool IsCheckInternetConnected, bool IsShowOfflineError)
            => BeginRunFunctionCore(IsRefreshDataSource, IsCheckInternetConnected, IsShowOfflineError);

        public bool EndRunFunction()
        {
            NextAuthorizationThreshold.IsResetDataSource = false;
            NextAuthorizationThreshold.IsRefreshDataSource = false;

            return true;
        }

        public void MultiThreadedPeriodicFunctions_InactiveMTPF(bool IsDispatcherTimerStop)
        {
            try
            {
                DispatcherTimerStartStopProcess(!IsDispatcherTimerStop, !IsDispatcherTimerStop);

                if (IsDispatcherTimerStop && ThreadTimerProcess != null && ThreadTimerProcess.IsAlive)
                { ThreadTimerProcess.Abort(); }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission); }
        }

        #endregion
    }
}
