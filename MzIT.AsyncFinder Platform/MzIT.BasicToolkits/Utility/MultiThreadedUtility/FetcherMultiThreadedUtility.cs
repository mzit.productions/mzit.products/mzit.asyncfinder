﻿using MzIT.BasicToolkits.Utility.FetcherHelpers;
using System;
using System.Threading;

namespace MzIT.BasicToolkits.Utility.MultiThreadedUtility
{
    public static class FetcherMultiThreadedUtility
    {
        #region Fields

        public delegate void MTF_Method();

        #endregion

        #region Public Methods

        public static bool Get_NextAuthorizationThreshold_IsEnabledCore(this NextAuthorizationThreshold source)
            => (source?.IsEnabledCore ?? false);

        public static bool MTF_StartThread_Invoke(MTF_Method method, bool IsMultiThreaded = true, ThreadPriority threadPriority = ThreadPriority.AboveNormal)
        {
            bool isValidation = false;

            try
            {
                if (IsMultiThreaded)
                {
                    Thread threadMTF_Method = new Thread(new ThreadStart(method))
                    {
                        Priority = threadPriority,
                        IsBackground = true
                    };

                    threadMTF_Method.Start();
                }
                else
                { method.Invoke(); }

                isValidation = true;
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex); }

            return isValidation;
        }

        public static Thread MTF_StartThread_Invoke_Pro(MTF_Method method, bool IsMultiThreaded = true, ThreadPriority threadPriority = ThreadPriority.AboveNormal)
        {
            Thread threadMTF_Method = null;

            try
            {
                if (IsMultiThreaded)
                {
                    threadMTF_Method = new Thread(new ThreadStart(method))
                    {
                        Priority = threadPriority,
                        IsBackground = true
                    };

                    threadMTF_Method.Start();
                }
                else
                { method.Invoke(); }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex); }

            return threadMTF_Method;
        }

        #endregion
    }
}
