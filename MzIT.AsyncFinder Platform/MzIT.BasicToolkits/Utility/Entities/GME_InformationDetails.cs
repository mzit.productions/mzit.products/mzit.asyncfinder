﻿using System;

namespace MzIT.BasicToolkits.Utility.Entities
{
    public class GME_InformationDetails
    {
        public string Caption { get; set; } = string.Empty;
        public string MessageInformation { get; set; } = string.Empty;
        public string ResultDescription { get; set; } = string.Empty;
        public string Result { get; set; } = string.Empty;
        public string ButtonFinalResult { get; set; } = string.Empty;
        public string ButtonFailureResult { get; set; } = string.Empty;
        public bool IsToUpper { get; set; } = true;
        public bool IsSuccessfullyInfo { get; set; } = true;
        public bool IsForceShowNotification { get; set; } = false;
        public Uri GoBackSource { get; set; }
    }
}
