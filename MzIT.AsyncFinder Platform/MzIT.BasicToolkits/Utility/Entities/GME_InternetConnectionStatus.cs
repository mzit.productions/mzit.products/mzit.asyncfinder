﻿namespace MzIT.BasicToolkits.Utility.Entities
{
    public class GME_InternetConnectionStatus
    {
        public bool Network_IsConnected { get; set; } = false;
        public string Network_Background_Online { get; set; } = "#FFF3F3F3";
        public string Network_Background_Offline { get; set; } = "#FFFFA8A8";
        public int Network_TryCatchConnectingAttempt { get; set; } = 4;
        public int Network_TryCatchConnectingAttemptCounter { get; set; } = 0;
        public bool Network_TryCatchIsShowOfflineError { get; set; } = false;
        public int Network_UITimePassedDispatcherTimerIntervalThreshold { get; set; } = 1 * 1000;
        public int Network_SpecialThresholdRefreshInternetConnectionStatus { get; set; } = 1 * 60 * 1000;

        public int Network_SpecialThresholdRefreshPostOfflineOrders { get; set; } = 15 * 60 * 1000;
    }
}
