﻿using System.ComponentModel;

namespace MzIT.BasicToolkits.Utility.Entities
{
    public static class GME_SpecialMethods
    {
        public enum SEE_ContentType
        {
            [SpecialGuid("c402da30-defb-4094-b886-2e8d04279492")]
            [SpecialDescription("Main Settings")]
            [Description("MainSettings.xml")]
            MainSettings = 0,

            [SpecialGuid("ebebe272-a001-4810-8c85-5c43f3cec472")]
            [SpecialDescription("Errors")]
            [Description("Errors.lebak")]
            LogError,

            [SpecialGuid("0d98a8c2-3692-4c9e-8dc1-6f2facaca916")]
            [SpecialDescription("Output Infos")]
            [Description("OutputInfos.oibak")]
            OutputInfo,
        }

        public enum SEE_DisplayDialogType
        {
            [Description("IsDefined")]
            IsDefined = 0,
            [Description("Default")]
            Default,
            [Description("Modal Dialogs")]
            ModalDialogs,
            [Description("Selected Items Window")]
            SelectedItemsWindow,
            [Description("Modal Selected Items Window")]
            ModalSelectedItemsWindow,
        }
    }
}
