﻿using System.Collections.Generic;
using System.Linq;

/// <!-- ListExtension 1.0.0.0 -->

namespace System
{
    public static class ListExtension
    {
        #region Methods

        public static IEnumerable<T> Safe<T>(this List<T> source)
        {
            if (source == null)
            { yield break; }

            foreach (var item in source)
            { yield return item; }
        }

        public static bool HasValue<T>(this List<T> source)
            => (source?.Count() > 0);

        public static bool IsEmpty<T>(this List<T> source)
            => !(source.Safe().Any());

        public static bool IsNullOrEmpty<T>(this List<T> source)
            => !(source.Safe().Any());

        public static List<T> ClearValue<T>(this List<T> source)
        {
            if (source == null)
            { source = new List<T>(); }
            source.Clear();

            return source;
        }

        #endregion
    }
}
