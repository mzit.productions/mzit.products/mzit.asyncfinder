﻿using System.Collections.Generic;
using System.Linq;

/// <!-- IListExtension 1.0.0.0 -->

namespace System
{
    public static class IListExtension
    {
        #region Methods

        public static IEnumerable<T> Safe<T>(this IList<T> source)
        {
            if (source == null)
            { yield break; }

            foreach (var item in source)
            { yield return item; }
        }

        public static bool HasValue<T>(this IList<T> source)
            => (source?.Count() > 0);

        public static bool IsEmpty<T>(this IList<T> source)
            => !(source.Safe().Any());

        public static bool IsNullOrEmpty<T>(this IList<T> source)
            => !(source.Safe().Any());

        public static IList<T> ClearValue<T>(this IList<T> source)
        {
            if (source == null)
            { source = new List<T>(); }
            source.Clear();

            return source;
        }

        #endregion
    }
}
