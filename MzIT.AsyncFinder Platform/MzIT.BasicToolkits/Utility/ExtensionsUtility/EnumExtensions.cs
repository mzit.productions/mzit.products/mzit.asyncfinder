﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;

/// <!-- EnumExtensions 1.0.0.0 -->

namespace System
{
    public static class EnumExtensions
    {
        /// <summary>
        /// Get Description of the Extension
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToDescriptionString<T>(this T value)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])value?.GetType()?.GetField(value?.ToString())?.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes?.Length > 0 ? attributes[0]?.Description : string.Empty;
        }

        /// <summary>
        /// Get Specially Description of the Extension
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToSpecialDescriptionString<T>(this T value)
        {
            SpecialDescriptionAttribute[] attributes = (SpecialDescriptionAttribute[])value?.GetType()?.GetField(value?.ToString())?.GetCustomAttributes(typeof(SpecialDescriptionAttribute), false);
            return attributes?.Length > 0 ? attributes[0]?.SpecialDescription : string.Empty;
        }

        /// <summary>
        /// Get Specially Color of the Extension
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToSpecialColorString<T>(this T value)
        {
            SpecialColorAttribute[] attributes = (SpecialColorAttribute[])value?.GetType()?.GetField(value?.ToString())?.GetCustomAttributes(typeof(SpecialColorAttribute), false);
            return attributes?.Length > 0 ? attributes[0]?.SpecialColor : string.Empty;
        }

        /// <summary>
        /// Get Specially Guid of the Extension
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToSpecialGuidString<T>(this T value)
        {
            SpecialGuidAttribute[] attributes = (SpecialGuidAttribute[])value?.GetType()?.GetField(value?.ToString())?.GetCustomAttributes(typeof(SpecialGuidAttribute), false);
            return attributes?.Length > 0 ? attributes[0]?.SpecialGuid : string.Empty;
        }

        /// <summary>
        /// Get Specially Value of the Extension
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToSpecialValueString<T>(this T value)
        {
            SpecialValueAttribute[] attributes = (SpecialValueAttribute[])value?.GetType()?.GetField(value?.ToString())?.GetCustomAttributes(typeof(SpecialValueAttribute), false);
            return attributes?.Length > 0 ? attributes[0]?.SpecialValue : string.Empty;
        }

        /// <summary>
        /// Get Specially View of the Extension
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToSpecialViewString<T>(this T value)
        {
            SpecialViewAttribute[] attributes = (SpecialViewAttribute[])value?.GetType()?.GetField(value?.ToString())?.GetCustomAttributes(typeof(SpecialViewAttribute), false);
            return attributes?.Length > 0 ? attributes[0]?.SpecialView : string.Empty;
        }

        /// <summary>
        /// Get Specially ViewModel of the Extension
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToSpecialViewModelString<T>(this T value)
        {
            SpecialViewModelAttribute[] attributes = (SpecialViewModelAttribute[])value?.GetType()?.GetField(value?.ToString())?.GetCustomAttributes(typeof(SpecialViewModelAttribute), false);
            return attributes?.Length > 0 ? attributes[0]?.SpecialViewModel : string.Empty;
        }

        /// <summary>
        /// Get Enum Member Index
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="element"></param>
        /// <returns></returns>
        public static int GetEnumMemberIndex<T>(T element)
            where T : struct
        {
            T[] values = (T[])Enum.GetValues(typeof(T));
            return Array.IndexOf(values, element);
        }

        /// <summary>
        /// Get Enum Value from string Value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="strValue"></param>
        /// <returns></returns>
        public static T GetEnumValue<T>(string strValue) where T : struct, IConvertible
        {
            Type enumType = typeof(T);
            if (!enumType.IsEnum)
            { throw new Exception("T must be an Enumeration type."); }
            return Enum.TryParse<T>(strValue, true, out T val) ? val : default(T);
        }

        /// <summary>
        /// Get Enum Value from int Value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="intValue"></param>
        /// <returns></returns>
        public static T GetEnumValue<T>(int intValue) where T : struct, IConvertible
        {
            Type enumType = typeof(T);
            if (!enumType.IsEnum)
            {
                throw new Exception("T must be an Enumeration type.");
            }
            return (T)Enum.ToObject(enumType, intValue);
        }

        public static IEnumerable<Enum> GetEnumValues(Enum enumeration)
        {
            return from f in enumeration.GetType().GetFields(BindingFlags.Static | BindingFlags.Public)
                   select (Enum)f.GetValue(enumeration);
        }

        /// <summary>
        /// Get Enum Value from int Value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="intValue"></param>
        /// <returns></returns>
        public static string GetDisplayEnumValue<T>(int intValue) where T : struct, IConvertible
        {
            try
            {
                Type enumType = typeof(T);
                if (!enumType.IsEnum)
                {
                    throw new Exception("T must be an Enumeration type.");
                }
                var enumValue = (T)Enum.ToObject(enumType, intValue);
                return enumValue.ToDescriptionString();
            }
            catch (Exception) { return "--"; /*GlobalizationFormats.DisableParameter;*/ }
        }

        /// <summary>
        /// Get Enum Value from int Value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="intValue"></param>
        /// <returns></returns>
        public static string GetDisplayEnumValue<T>(Nullable<int> intValue) where T : struct, IConvertible
        {
            try
            {
                Type enumType = typeof(T);
                if (!enumType.IsEnum)
                {
                    throw new Exception("T must be an Enumeration type.");
                }
                var enumValue = (T)Enum.ToObject(enumType, intValue);
                return enumValue.ToDescriptionString();
            }
            catch (Exception) { return "--"; /*GlobalizationFormats.DisableParameter;*/ }
        }

        /// <summary>
        /// Client Utils _ Is Enum Valid
        /// </summary>
        /// <param name="enumValue"></param>
        /// <param name="value"></param>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <returns></returns>
        public static bool ClientUtils_IsEnumValid(Enum enumValue, int value, int minValue, int maxValue) =>
            ((value >= minValue) && (value <= maxValue));

        /// <summary>
        /// Windows Forms Utils _ Enum Validator _ Is Enum With in Shifted Range
        /// </summary>
        /// <param name="enumValue"></param>
        /// <param name="numBitsToShift"></param>
        /// <param name="minValAfterShift"></param>
        /// <param name="maxValAfterShift"></param>
        /// <returns></returns>
        private static bool WindowsFormsUtils_EnumValidator_IsEnumWithinShiftedRange(Enum enumValue, int numBitsToShift, int minValAfterShift, int maxValAfterShift)
        {
            int num = Convert.ToInt32(enumValue, CultureInfo.InvariantCulture);
            int num2 = num >> numBitsToShift;
            if ((num2 << numBitsToShift) != num)
            { return false; }
            return ((num2 >= minValAfterShift) && (num2 <= maxValAfterShift));
        }

        /// <summary>
        /// A convenience method to XOR two System.Enums, assuming the underlying type is <= 64 bits wide
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Enum Xor(this Enum a, Enum b)
        {
            if (Enum.GetUnderlyingType(a.GetType()) == typeof(ulong))
            {
                return (Enum)Enum.ToObject(a.GetType(), Convert.ToUInt64(a) ^ Convert.ToUInt64(b));
            }
            else
            {
                //anything smaller than an uint64 will fit in an int64
                return (Enum)Enum.ToObject(a.GetType(), Convert.ToInt64(a) ^ Convert.ToInt64(b));
            }
        }
    }
}
