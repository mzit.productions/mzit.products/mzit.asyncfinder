﻿using MzIT.BasicToolkits.Utility.FetcherHelpers;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Media;

/// <!-- StringExtensions 1.0.0.0 -->

namespace System
{
    public static class StringExtensions
    {
        #region Static Fields

        public static string DefaultParameter { get; set; } = GlobalizationFormats.UnknownParameter;
        public static string UnknownParameter { get; set; } = GlobalizationFormats.UnknownParameter;
        public static string UnknownParameterPro { get; set; } = GlobalizationFormats.UnknownParameterPro;

        #endregion

        #region Methods

        public static string ToStringDisplay(this string StringValue)
        {
            if (string.IsNullOrWhiteSpace(StringValue))
            { return string.Empty; }
            else { return StringValue.Trim(); }
        }

        public static string ToValue(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            { return string.Empty; }
            else { return source.Trim(); }
        }

        public static string ToValueOrDefault(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            { return StringExtensions.DefaultParameter; }
            else { return source.Trim(); }
        }

        public static string ToValueOrUnknown(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            { return StringExtensions.UnknownParameter; }
            else { return source.Trim(); }
        }

        public static string ToValueOrUnknownPro(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            { return StringExtensions.UnknownParameterPro; }
            else { return source.Trim(); }
        }

        public static string ToFetcherDataContents(this string value)
        {
            try
            {
                return
                    (value == null ? GlobalizationFormats.DisableParameter :
                    value.Trim()
                    .Replace(' ', '_')
                    .Replace('(', '_').Replace(')', '_')
                    .Replace('-', '_').Replace('.', '_').Replace(',', '_').Replace(';', '_').Replace('"', '_')
                    .Replace('*', '_').Replace('?', '_').Replace('؟', '_').Replace('<', '_').Replace('>', '_').Replace('|', '_')
                    .Replace("//", "_").Replace('/', '_')
                    .Replace(@"\\", "_").Replace(@"\", "_")
                    .Replace("&", "and")
                    .Replace("___", "_").Replace("__", "_"));
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission); }

            return value;
        }

        /// <summary>
        /// Delete the first n lines in a string
        /// </summary>
        /// <param name="text"></param>
        /// <param name="linesCount"></param>
        /// <returns></returns>
        public static string ToRemoveFirstLines(this string source, int linesCount)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(source))
                {
                    var lines = Regex.Split(source, "\r\n|\r|\n").Skip(linesCount);
                    return string.Join(Environment.NewLine, lines.ToArray());
                }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }

            return source;
        }

        /// <summary>
        /// Delete the first n lines in a string
        /// </summary>
        /// <param name="source"></param>
        /// <param name="linesCount"></param>
        /// <returns></returns>
        public static string ToRemoveEnvironmentFirstLines(this string source, int? linesCount)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(source))
                {
                    var lines = Regex.Split(source, "\r\n|\r|\n").Skip(linesCount ?? 2);

                    if (lines?.Count() == 1)
                    { source = string.Join(Environment.NewLine, lines.ToArray()).Trim(); }
                }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }

            return source;
        }

        public static string FirstCharToUpper(this string input, bool forceOthersToLower = true)
        {
            try
            {
                switch (input)
                {
                    case null:
                        throw new ArgumentNullException(nameof(input));
                    case "":
                        throw new ArgumentException($"{nameof(input)} cannot be empty", nameof(input));
                    default:
                        {
                            if (forceOthersToLower)
                            { return input.First().ToString().ToUpper() + input.Substring(1).ToLower(); }
                            else
                            { return input.First().ToString().ToUpper() + input.Substring(1); }
                        }
                }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }

            return input;
        }

        public static Color StringToColor(this string hashColor)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(hashColor) && hashColor.Trim().Length > 0)
                { return (Color)(ColorConverter.ConvertFromString(hashColor)); }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }

            return Colors.Transparent;
        }

        public static SolidColorBrush StringToSolidColorBrush(this string hashColor)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(hashColor) && hashColor.Trim().Length > 0)
                { return (SolidColorBrush)(new BrushConverter().ConvertFromString(hashColor)); }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }

            return Brushes.Transparent;
        }

        #endregion
    }
}
