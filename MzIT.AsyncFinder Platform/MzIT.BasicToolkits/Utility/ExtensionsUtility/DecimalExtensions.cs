﻿using MzIT.BasicToolkits.Utility.FetcherHelpers;

/// <!-- DecimalExtensions 1.0.0.0 -->

namespace System
{
    public static class DecimalExtensions
    {
        #region Fields

        /// <summary>
        /// Get OR Set Number of Decimal Places To Use in Currency/Numeric Values
        /// Default = 2
        /// </summary>
        public static int DecimalDigits { get; set; } = GlobalizationFormats.CurrencyDecimalDigits;

        /// <summary>
        /// Get OR Set Format of Decimal Places To Use in Currency/Numeric Values
        /// Default = N2
        /// </summary>
        public static string DecimalDigitsFormat { get; set; } = GlobalizationFormats.CurrencyDecimalDigitsFormat;

        /// <summary>
        /// Get OR Set Format of Decimal Places To Use in Currency/Numeric Quantity Values
        /// Default = 0.##
        /// </summary>
        public static string DecimalDigitsFormatQuantity { get; set; } = GlobalizationFormats.CurrencyDecimalDigitsFormatQuantity;

        #endregion

        #region Methods

        public static string ToStringDisplay(this decimal? source)
            => decimal.Round((source ?? 0), DecimalDigits).ToString(DecimalDigitsFormat);

        public static string ToStringDisplay(
            this decimal? source,
            string decimalDigitsFormat = GlobalizationFormats.CurrencyDecimalDigitsFormat,
            int decimals = GlobalizationFormats.CurrencyDecimalDigits)
        {
            if (source == null)
            { source = new decimal(0); }

            if (string.IsNullOrEmpty(decimalDigitsFormat))
            { decimalDigitsFormat = string.Empty; }

            if (decimals < 0)
            { decimals = 0; }

            return (decimal.Round((source ?? 0), decimals).ToString(decimalDigitsFormat));
        }

        public static string ToStringDisplayQuantity(this int? source)
            => decimal.Round((source ?? 0), DecimalDigits).ToString(DecimalDigitsFormatQuantity);

        public static string ToStringDisplayQuantity(this decimal? source)
            => decimal.Round((source ?? 0), DecimalDigits).ToString(DecimalDigitsFormatQuantity);

        public static string ToStringDisplayQuantity(
            this decimal? source,
            string decimalDigitsFormat = GlobalizationFormats.CurrencyDecimalDigitsFormatQuantity,
            int decimals = GlobalizationFormats.CurrencyDecimalDigits)
        {
            if (source == null)
            { source = new decimal(0); }

            if (string.IsNullOrEmpty(decimalDigitsFormat))
            { decimalDigitsFormat = string.Empty; }

            if (decimals < 0)
            { decimals = 0; }

            return (decimal.Round((source ?? 0), decimals).ToString(decimalDigitsFormat));
        }

        public static decimal? ToValue(this decimal? source)
        {
            if (source.HasValue)
            { return Math.Round(source.Value, DecimalDigits + 1); }
            return null;
        }

        public static decimal? ToValueOrDefault(this decimal? source)
            => Math.Round(source.Value, DecimalDigits);

        public static bool IsDisplayNumericInputValue(this decimal? source, bool IsDisplayNumericInputZeroValue)
            => (IsDisplayNumericInputZeroValue || (source.Value != 0));

        public static string ToStringDisplay(this decimal source)
            => decimal.Round(source, DecimalDigits).ToString(DecimalDigitsFormat);

        public static string ToStringDisplay(
            this decimal source,
            string decimalDigitsFormat = GlobalizationFormats.CurrencyDecimalDigitsFormat,
            int decimals = GlobalizationFormats.CurrencyDecimalDigits)
        {
            if (string.IsNullOrEmpty(decimalDigitsFormat))
            { decimalDigitsFormat = string.Empty; }

            if (decimals < 0)
            { decimals = 0; }

            return (decimal.Round(source, decimals).ToString(decimalDigitsFormat));
        }

        public static string ToStringDisplayQuantity(this int source)
            => decimal.Round(source, DecimalDigits).ToString(DecimalDigitsFormatQuantity);

        public static string ToStringDisplayQuantity(this decimal source)
            => decimal.Round(source, DecimalDigits).ToString(DecimalDigitsFormatQuantity);

        public static string ToStringDisplayQuantity(
            this decimal source,
            string decimalDigitsFormat = GlobalizationFormats.CurrencyDecimalDigitsFormatQuantity,
            int decimals = GlobalizationFormats.CurrencyDecimalDigits)
        {
            if (string.IsNullOrEmpty(decimalDigitsFormat))
            { decimalDigitsFormat = string.Empty; }

            if (decimals < 0)
            { decimals = 0; }

            return (decimal.Round(source, decimals).ToString(decimalDigitsFormat));
        }

        public static decimal ToValue(this decimal source)
            => Math.Round(source, DecimalDigits + 1);

        public static decimal ToValueOrDefault(this decimal source)
            => Math.Round(source, DecimalDigits);

        public static bool IsDisplayNumericInputValue(this decimal source, bool IsDisplayNumericInputZeroValue)
            => (IsDisplayNumericInputZeroValue || (source != 0));

        #endregion
    }
}
