﻿using System.Collections.Generic;
using System.Linq;

/// <!-- IEnumerableExtension 1.0.0.0 -->

namespace System
{
    public static class IEnumerableExtension
    {
        #region Methods

        public static IEnumerable<T> Safe<T>(this IEnumerable<T> source)
        {
            if (source == null)
            { yield break; }

            foreach (var item in source)
            { yield return item; }
        }

        public static bool HasValue<T>(this IEnumerable<T> source)
            => (source?.Count() > 0);

        public static bool IsEmpty<T>(this IEnumerable<T> source)
            => !(source.Safe().Any());

        public static bool IsNullOrEmpty<T>(this IEnumerable<T> source)
            => !(source.Safe().Any());

        public static IEnumerable<T> ClearValue<T>(this IEnumerable<T> source)
        {
            if (source == null)
            { source = new List<T>(); }
            source.ToList().ClearValue();

            return source;
        }

        public static IEnumerable<List<T>> SplitList<T>(List<T> source, int nSize)
        {
            if (source.IsNullOrEmpty())
            { yield return source; }

            for (int i = 0; i < source.Count; i += nSize)
            {
                yield return source.GetRange(i, Math.Min(nSize, source.Count - i));
            }
        }

        #endregion
    }
}
