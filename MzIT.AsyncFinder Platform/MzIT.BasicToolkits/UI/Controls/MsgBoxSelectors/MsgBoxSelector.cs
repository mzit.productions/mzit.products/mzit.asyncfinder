﻿//using MzIT.BasicToolkits.UI.Controls.ModalDialogs;
using MzIT.BasicToolkits.UI.Controls.MsgBoxSelectors.Models;
using MzIT.BasicToolkits.Utility.FetcherHelpers;
using System;
using System.ComponentModel;
using System.Windows;
using static MzIT.BasicToolkits.Utility.Entities.GME_SpecialMethods;

namespace MzIT.BasicToolkits.UI.Controls.MsgBoxSelectors
{
    public static class MsgBoxSelector
    {
        #region Fields

        private static bool IsMetroMsgBoxSelector { get; set; } = true;
        public static bool IsAutoConfirmationMessages { get; set; } = true;
        public static bool IsRemoveEnvironmentFirstLinesMessages { get; set; } = true;
        //public static ModalDialog ModalDialog { get; set; } = null;
        private static bool IsConsoleApp { get; set; } = true;

        #endregion

        #region MsgBoxSelector Helpers - Private

        private static MessageBoxImage LEE_MetroMsgBoxSelectorTypeToMessageBoxImage(LEE_MetroMsgBoxSelectorType metroMsgBoxSelectorType)
        {
            MessageBoxImage messageBoxImage = MessageBoxImage.None;

            try
            {
                switch (metroMsgBoxSelectorType)
                {
                    case LEE_MetroMsgBoxSelectorType.None:
                    case LEE_MetroMsgBoxSelectorType.Custom:
                        messageBoxImage = MessageBoxImage.None;
                        break;
                    case LEE_MetroMsgBoxSelectorType.Hand:
                        messageBoxImage = MessageBoxImage.Hand;
                        break;
                    case LEE_MetroMsgBoxSelectorType.Stop:
                    case LEE_MetroMsgBoxSelectorType.QuestionStop:
                        messageBoxImage = MessageBoxImage.Stop;
                        break;
                    case LEE_MetroMsgBoxSelectorType.Error:
                        messageBoxImage = MessageBoxImage.Error;
                        break;
                    case LEE_MetroMsgBoxSelectorType.Question:
                    case LEE_MetroMsgBoxSelectorType.QuestionConfirm:
                    case LEE_MetroMsgBoxSelectorType.QuestionPrint:
                        messageBoxImage = MessageBoxImage.Question;
                        break;
                    case LEE_MetroMsgBoxSelectorType.Exclamation:
                        messageBoxImage = MessageBoxImage.Exclamation;
                        break;
                    case LEE_MetroMsgBoxSelectorType.Warning:
                    case LEE_MetroMsgBoxSelectorType.QuestionWarning:
                    case LEE_MetroMsgBoxSelectorType.QuestionDelete:
                        messageBoxImage = MessageBoxImage.Warning;
                        break;
                    case LEE_MetroMsgBoxSelectorType.Asterisk:
                        messageBoxImage = MessageBoxImage.Asterisk;
                        break;
                    case LEE_MetroMsgBoxSelectorType.Information:
                    case LEE_MetroMsgBoxSelectorType.Print:
                    case LEE_MetroMsgBoxSelectorType.Success:
                    case LEE_MetroMsgBoxSelectorType.Confirm:
                        messageBoxImage = MessageBoxImage.Information;
                        break;
                    case LEE_MetroMsgBoxSelectorType.TryCatchError:
                    case LEE_MetroMsgBoxSelectorType.ValidationError:
                    case LEE_MetroMsgBoxSelectorType.Failure:
                        messageBoxImage = MessageBoxImage.Error;
                        break;
                }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission); }

            return messageBoxImage;
        }

        private static MessageBoxResult SetNewMetroMsgBoxSelectorUC(
            LEE_MetroMsgBoxSelectorType metroMsgBoxSelectorType,
            string caption,
            string message,
            string result,
            MessageBoxButton? button,
            LEE_MetroMsgBoxSelectorType? image,
            MessageBoxResult? defaultResult,
            SEE_DisplayDialogType displayDialogType)
        {
            MessageBoxResult oMessageBoxResult = MessageBoxResult.None;

            try
            {
                MetroMsgBoxSelector oMetroMsgBoxSelector = new MetroMsgBoxSelector
                {
                    Caption = caption,
                    Message = message,
                    Result = result,

                    MetroMessageBoxButton = button,
                    MetroMessageBoxImage = image,
                    DefaultMessageBoxResult = defaultResult,

                    MetroMsgBoxSelectorType = metroMsgBoxSelectorType
                };

                if (displayDialogType == SEE_DisplayDialogType.Default)
                    displayDialogType = SEE_DisplayDialogType.ModalSelectedItemsWindow;

                var showHandlerDialogResult = SpecialModels.ShowHandlerDialog(oMetroMsgBoxSelector, displayDialogType, null/*, ModalDialog*/);

                if (showHandlerDialogResult ?? false)
                {
                    switch (button)
                    {
                        case MessageBoxButton.OK:
                        case MessageBoxButton.OKCancel:
                            oMessageBoxResult = MessageBoxResult.OK;
                            break;
                        case MessageBoxButton.YesNo:
                        case MessageBoxButton.YesNoCancel:
                            oMessageBoxResult = MessageBoxResult.Yes;
                            break;
                    }
                }
                else
                {
                    switch (button)
                    {
                        case MessageBoxButton.OK:
                            oMessageBoxResult = MessageBoxResult.OK;
                            break;
                        case MessageBoxButton.OKCancel:
                        case MessageBoxButton.YesNoCancel:
                            oMessageBoxResult = MessageBoxResult.Cancel;
                            break;
                        case MessageBoxButton.YesNo:
                            oMessageBoxResult = MessageBoxResult.No;
                            break;
                    }
                }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission); }

            return oMessageBoxResult;
        }

        #endregion

        #region MsgBoxSelector Methods - Private

        private static MessageBoxResult ShowMsgBoxSelector(
            LEE_MetroMsgBoxSelectorType metroMsgBoxSelectorType,
            string text,
            string caption = null,
            bool? isAutoConfirmation = false,
            bool? isRemoveEnvironmentFirstLines = null,
            int? isRemoveEnvironmentFirstLinesCount = 2,
            MessageBoxButton? button = MessageBoxButton.OK,
            LEE_MetroMsgBoxSelectorType? image = LEE_MetroMsgBoxSelectorType.Information,
            MessageBoxResult? defaultResult = MessageBoxResult.OK,
            bool? isAutoClosing = null,
            SEE_DisplayDialogType displayDialogType = SEE_DisplayDialogType.Default)
        {
            MessageBoxResult messageBoxResult = MessageBoxResult.None;

            try
            {
                switch (metroMsgBoxSelectorType)
                {
                    case LEE_MetroMsgBoxSelectorType.None:
                    case LEE_MetroMsgBoxSelectorType.Hand:
                    case LEE_MetroMsgBoxSelectorType.Stop:
                    case LEE_MetroMsgBoxSelectorType.Error:
                    case LEE_MetroMsgBoxSelectorType.Exclamation:
                    case LEE_MetroMsgBoxSelectorType.Warning:
                    case LEE_MetroMsgBoxSelectorType.Asterisk:
                    case LEE_MetroMsgBoxSelectorType.Information:
                    case LEE_MetroMsgBoxSelectorType.Success:
                    case LEE_MetroMsgBoxSelectorType.Failure:
                    case LEE_MetroMsgBoxSelectorType.Print:
                        SetMsgBoxElements(ref image, ref button, ref defaultResult, metroMsgBoxSelectorType);
                        break;
                    case LEE_MetroMsgBoxSelectorType.Question:
                    case LEE_MetroMsgBoxSelectorType.QuestionPrint:
                        SetMsgBoxElements(ref image, ref button, ref defaultResult, metroMsgBoxSelectorType, MessageBoxButton.YesNo, MessageBoxResult.Yes);
                        break;
                    case LEE_MetroMsgBoxSelectorType.TryCatchError:
                    case LEE_MetroMsgBoxSelectorType.ValidationError:
                        SetMsgBoxElements(ref image, ref button, ref defaultResult, metroMsgBoxSelectorType);
                        if (isRemoveEnvironmentFirstLines == null) isRemoveEnvironmentFirstLines = IsRemoveEnvironmentFirstLinesMessages;
                        break;
                    case LEE_MetroMsgBoxSelectorType.Confirm:
                        SetMsgBoxElements(ref image, ref button, ref defaultResult, metroMsgBoxSelectorType);
                        if (isAutoConfirmation == null) isAutoConfirmation = IsAutoConfirmationMessages;
                        break;
                    case LEE_MetroMsgBoxSelectorType.QuestionStop:
                    case LEE_MetroMsgBoxSelectorType.QuestionWarning:
                    case LEE_MetroMsgBoxSelectorType.QuestionDelete:

                        SetMsgBoxElements(ref image, ref button, ref defaultResult, metroMsgBoxSelectorType, MessageBoxButton.YesNo, MessageBoxResult.No);
                        break;
                    case LEE_MetroMsgBoxSelectorType.QuestionConfirm:
                        SetMsgBoxElements(ref image, ref button, ref defaultResult, metroMsgBoxSelectorType, MessageBoxButton.YesNo, MessageBoxResult.Yes);
                        if (isAutoConfirmation == null) isAutoConfirmation = IsAutoConfirmationMessages;
                        break;
                    case LEE_MetroMsgBoxSelectorType.Custom:
                        SetMsgBoxElements(ref image, ref button, ref defaultResult, LEE_MetroMsgBoxSelectorType.None);
                        break;
                }

                if (text == null) text = string.Empty;

                if (isRemoveEnvironmentFirstLines ?? false)
                    text = text.ToRemoveEnvironmentFirstLines(isRemoveEnvironmentFirstLinesCount);

                if (IsConsoleApp)
                {
                    Logging.LogOutputInfo(
                        string.Format("{0}: {1}", caption, text));
                }
                else if (isAutoConfirmation ?? false)
                {
                    messageBoxResult = MessageBoxResult.Yes;
                }
                else
                {
                    if (IsMetroMsgBoxSelector)
                    {
                        if (Application.Current != null)
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                messageBoxResult = SetNewMetroMsgBoxSelectorUC(
                                    metroMsgBoxSelectorType, caption, text, string.Empty, button, image, defaultResult, displayDialogType);
                            });
                        }
                        else
                        {
                            messageBoxResult = SetNewMetroMsgBoxSelectorUC(
                                metroMsgBoxSelectorType, caption, text, string.Empty, button, image, defaultResult, displayDialogType);
                        }
                    }
                    else
                    {
                        var oMessageBoxImage = LEE_MetroMsgBoxSelectorTypeToMessageBoxImage(metroMsgBoxSelectorType);

                        if (caption == null || caption.Trim() == "" || caption.Trim() == GlobalizationFormats.DisableParameter)
                            caption = metroMsgBoxSelectorType.ToDescriptionString();

                        messageBoxResult = MessageBox.Show(
                            text, caption, button ?? MessageBoxButton.OK, oMessageBoxImage, defaultResult ?? MessageBoxResult.OK);
                    }
                }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission); }

            return messageBoxResult;
        }

        private static void SetMsgBoxElements(
            ref LEE_MetroMsgBoxSelectorType? image,
            ref MessageBoxButton? button,
            ref MessageBoxResult? defaultResult,
            LEE_MetroMsgBoxSelectorType imageValue,
            MessageBoxButton? buttonValue = MessageBoxButton.OK,
            MessageBoxResult? defaultResultValue = MessageBoxResult.OK)
        {
            image = image ?? imageValue;
            button = button ?? buttonValue;
            defaultResult = defaultResult ?? defaultResultValue;
        }

        #endregion

        #region MsgBoxSelector Methods - Public

        public static MessageBoxResult ShowDialog(
            LEE_MetroMsgBoxSelectorType metroMsgBoxSelectorType,
            string text,
            string caption = null,
            bool? isAutoConfirmation = null,
            bool? isRemoveEnvironmentFirstLines = null,
            int? isRemoveEnvironmentFirstLinesCount = null,
            MessageBoxButton? button = null,
            LEE_MetroMsgBoxSelectorType? image = null,
            MessageBoxResult? defaultResult = null,
            bool? isAutoClosing = null,
            SEE_DisplayDialogType displayDialogType = SEE_DisplayDialogType.Default)
        {
            return
                ShowMsgBoxSelector(
                    metroMsgBoxSelectorType,
                    text,
                    caption,
                    isAutoConfirmation,
                    isRemoveEnvironmentFirstLines,
                    isRemoveEnvironmentFirstLinesCount,
                    button,
                    image,
                    defaultResult,
                    isAutoClosing);
        }

        #endregion
    }

    #region MsgBoxSelector Box Methods

    public enum LEE_MetroMsgBoxSelectorType
    {
        #region Default Types

        [SpecialColor("#FF616161")]
        [SpecialDescription("None.png")]
        [Description("Info")]
        None = 0,
        [SpecialColor("#FF1E88E5")]
        [SpecialDescription("Hand.png")]
        [Description("Info")]
        Hand,
        [SpecialColor("#FFF44336")]
        [SpecialDescription("Stop.png")]
        [Description("Stop")]
        Stop,
        [SpecialColor("#FFF44336")]
        [SpecialDescription("Error.png")]
        [Description("Error")]
        Error,
        [SpecialColor("#FF1E88E5")]
        [SpecialDescription("Question.png")]
        [Description("Question")]
        Question,
        [SpecialColor("#FF1E88E5")]
        [SpecialDescription("Exclamation.png")]
        [Description("Exclamation")]
        Exclamation,
        [SpecialColor("#FFF9A825")]
        [SpecialDescription("Warning.png")]
        [Description("Warning")]
        Warning,
        [SpecialColor("#FF1E88E5")]
        [SpecialDescription("Asterisk.png")]
        [Description("Asterisk")]
        Asterisk,
        [SpecialColor("#FF1E88E5")]
        [SpecialDescription("Information.png")]
        [Description("Information")]
        Information,

        #endregion

        #region Special

        [SpecialColor("#FFF44336")]
        [SpecialDescription("TryCatchError.png")]
        [Description("Error")]
        TryCatchError,
        [SpecialColor("#FFF44336")]
        [SpecialDescription("ValidationError.png")]
        [Description("Validation Error")]
        ValidationError,
        [SpecialColor("#FF43A047")]
        [SpecialDescription("Success.png")]
        [Description("Success")]
        Success,
        [SpecialColor("#FF43A047")]
        [SpecialDescription("Confirm.png")]
        [Description("Confirm")]
        Confirm,
        [SpecialColor("#FFF44336")]
        [SpecialDescription("Failure.png")]
        [Description("Failure")]
        Failure,
        [SpecialColor("#FF1E88E5")]
        [SpecialDescription("Print.png")]
        [Description("Print")]
        Print,

        #endregion

        #region Combine

        [SpecialColor("#FFF44336")]
        [SpecialDescription("QuestionStop.png")]
        [Description("Confirm")]
        QuestionStop,
        [SpecialColor("#FFF9A825")]
        [SpecialDescription("QuestionWarning.png")]
        [Description("Warning")]
        QuestionWarning,
        [SpecialColor("#FF1E88E5")]
        [SpecialDescription("QuestionConfirm.png")]
        [Description("Confirm")]
        QuestionConfirm,
        [SpecialColor("#FFF44336")]
        [SpecialDescription("QuestionDelete.png")]
        [Description("Confirm")]
        QuestionDelete,
        [SpecialColor("#FFF9A825")]
        [SpecialDescription("QuestionPrint.png")]
        [Description("Print")]
        QuestionPrint,

        #endregion

        #region Custom by user

        [SpecialColor("#FF616161")]
        [SpecialDescription("Info.png")]
        [Description("Info")]
        Custom,

        #endregion
    }

    public enum LEE_MetroMsgBoxSelectorDisplayStyle
    {
        None,
        Text,
        Image,
        ImageAndText
    }

    public enum LEE_MsgBoxSelectorGeneralMessage
    {
        [SpecialDescription("The registration process has been successfully completed.")]
        RegistrationProcess_Primary,
        [SpecialDescription("The registration step has been successfully completed.")]
        RegistrationProcess_Secondary,
        [SpecialDescription("Editing process was successful.")]
        RegistrationProcess_Edit,
        [SpecialDescription("The deletion process was completed successfully.")]
        RegistrationProcess_Delete,
    }

    #endregion
}
