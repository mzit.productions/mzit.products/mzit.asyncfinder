﻿//using MzIT.BasicToolkits.UI.Controls.ModalDialogs;
using MzIT.BasicToolkits.Utility.FetcherHelpers;
using System;
using System.Windows;
using System.Windows.Media;
using static MzIT.BasicToolkits.Utility.Entities.GME_SpecialMethods;

namespace MzIT.BasicToolkits.UI.Controls.MsgBoxSelectors.Models
{
    internal static class SpecialModels
    {
        public static bool? ShowHandlerDialog(
            FrameworkElement oFrameworkElement,
            SEE_DisplayDialogType DisplayDialogType = SEE_DisplayDialogType.Default,
            FrameworkElement oParentFrameworkElement = null
            //, ModalDialog ModalDialog = null
            )
        {
            bool? dialogResult = null;

            try
            {
                if (oFrameworkElement == null)
                { return null; };

                if (DisplayDialogType == SEE_DisplayDialogType.Default)
                { DisplayDialogType = SEE_DisplayDialogType.ModalSelectedItemsWindow; }

                //if (DisplayDialogType == SEE_DisplayDialogType.ModalDialogs && ModalDialog == null)
                //{ DisplayDialogType = SEE_DisplayDialogType.ModalSelectedItemsWindow; }

                switch (DisplayDialogType)
                {
                    case SEE_DisplayDialogType.ModalDialogs:
                        {
                            //if (ModalDialog != null)
                            //{
                            //    if ((oFrameworkElement as MetroMsgBoxSelector) != null)
                            //    { (oFrameworkElement as MetroMsgBoxSelector).UserControl_Loaded(); }

                            //    dialogResult = ModalDialog.ShowHandlerDialog(oFrameworkElement);

                            //    if ((oFrameworkElement as MetroMsgBoxSelector) != null)
                            //    { (oFrameworkElement as MetroMsgBoxSelector).UserControl_Unloaded(); }
                            //}

                            break;
                        }
                    case SEE_DisplayDialogType.SelectedItemsWindow:
                        {
                            MetroMsgBoxWindow oMetroMsgBoxWindow = new MetroMsgBoxWindow
                            {
                                Content = oFrameworkElement
                            };
                            oFrameworkElement.Tag = oMetroMsgBoxWindow;
                            oMetroMsgBoxWindow.AllowsTransparency = true;
                            oMetroMsgBoxWindow.Background = HelperFunctions.StringToSolidColorBrush("#FFF3F3F3");

                            if (Application.Current != null)
                            {
                                oMetroMsgBoxWindow.Owner = Application.Current.MainWindow;
                            }

                            dialogResult = oMetroMsgBoxWindow.ShowDialog();

                            oFrameworkElement.Tag = null;
                            oMetroMsgBoxWindow.WindowDialogChildrenClear();
                            oMetroMsgBoxWindow = null;
                            break;
                        }
                    case SEE_DisplayDialogType.ModalSelectedItemsWindow:
                        {
                            MetroMsgBoxWindow oMetroMsgBoxWindow = new MetroMsgBoxWindow(oFrameworkElement);
                            oFrameworkElement.Tag = oMetroMsgBoxWindow;
                            oMetroMsgBoxWindow.AllowsTransparency = true;
                            oMetroMsgBoxWindow.Background = Brushes.Transparent;

                            if (Application.Current != null)
                            {
                                oMetroMsgBoxWindow.Owner = Application.Current.MainWindow;
                            }

                            if ((oFrameworkElement as MetroMsgBoxSelector) != null)
                            { (oFrameworkElement as MetroMsgBoxSelector).ParentMetroMsgBoxWindow = oMetroMsgBoxWindow; }

                            dialogResult = oMetroMsgBoxWindow.ShowDialog();

                            if ((oFrameworkElement as MetroMsgBoxSelector) != null)
                            { (oFrameworkElement as MetroMsgBoxSelector).ParentMetroMsgBoxWindow = null; }

                            oFrameworkElement.Tag = null;
                            oMetroMsgBoxWindow.WindowDialogChildrenClear();
                            oMetroMsgBoxWindow = null;

                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                dialogResult = null;
                FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission);
            }

            return dialogResult;
        }

        public static bool? HideHandlerDialog(
            FrameworkElement oFrameworkElement,
            bool dialogResult,
            SEE_DisplayDialogType DisplayDialogType = SEE_DisplayDialogType.Default)
        {
            bool IsRefresh = false;

            try
            {
                if (oFrameworkElement == null)
                { return null; };

                if (oFrameworkElement.Parent is MetroMsgBoxWindow parentWindow)
                {
                    if (!(parentWindow.DialogResult ?? false))
                    { parentWindow.DialogResult = dialogResult; }

                    parentWindow.Close();
                }
                else if ((oFrameworkElement.Tag as MetroMsgBoxWindow) != null)
                {
                    if (oFrameworkElement.Tag is MetroMsgBoxWindow parentModalMetroMsgBoxWindow)
                    {
                        if (!(parentModalMetroMsgBoxWindow.DialogResult ?? false))
                        { parentModalMetroMsgBoxWindow.DialogResult = dialogResult; }

                        parentModalMetroMsgBoxWindow.Close();
                    }
                }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission, FetcherTryCatch.FTC_PermissionType.FullPermission); }

            return IsRefresh;
        }
    }
}
