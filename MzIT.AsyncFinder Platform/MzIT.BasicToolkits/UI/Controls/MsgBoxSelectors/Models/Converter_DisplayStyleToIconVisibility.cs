﻿using MzIT.BasicToolkits.Utility.FetcherHelpers;
using System;
using System.Windows;
using System.Windows.Data;

namespace MzIT.BasicToolkits.UI.Controls.MsgBoxSelectors
{
    public class Converter_DisplayStyleToIconVisibility : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            Visibility returnValue = Visibility.Collapsed;

            try
            {
                if (value != null && value is LEE_MetroMsgBoxSelectorDisplayStyle)
                {
                    if ((LEE_MetroMsgBoxSelectorDisplayStyle)value == LEE_MetroMsgBoxSelectorDisplayStyle.Image
                        || (LEE_MetroMsgBoxSelectorDisplayStyle)value == LEE_MetroMsgBoxSelectorDisplayStyle.ImageAndText)
                    {
                        returnValue = Visibility.Visible;
                    }
                }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }

            return returnValue;
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException(
                "No need to convert from IconVisibility back to DisplayStyle");
        }
    }
}
