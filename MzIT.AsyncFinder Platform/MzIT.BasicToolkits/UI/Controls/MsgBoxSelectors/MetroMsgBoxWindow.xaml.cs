﻿using MzIT.BasicToolkits.Utility.FetcherHelpers;
using System;
using System.Windows;

namespace MzIT.BasicToolkits.UI.Controls.MsgBoxSelectors
{
    /// <summary>
    /// Interaction logic for MetroMsgBoxWindow.xaml
    /// </summary>
    public partial class MetroMsgBoxWindow : Window
    {
        public MetroMsgBoxWindow()
        {
            InitializeComponent();
        }

        public MetroMsgBoxWindow(FrameworkElement oFrameworkElement)
            : this()
        {
            try
            {
                windowDialog.Children.Clear();
                windowDialog.Children.Add(oFrameworkElement);
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex); }
        }

        #region Fields

        #endregion

        #region Methods

        public void WindowDialogChildrenClear()
        {
            try
            {
                windowDialog.Children.Clear();
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex); }
        }

        #endregion

        /// <summary>
        /// Load the main window of this application for SelectedItems
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MetroMsgBoxWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {

            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex); }
        }

        /// <summary>
        /// Unload the main window of this application for SelectedItems
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MetroMsgBoxWindow_Unloaded(object sender, RoutedEventArgs e)
        {
            try
            {

            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex); }
        }
    }
}
