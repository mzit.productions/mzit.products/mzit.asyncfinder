﻿using MzIT.BasicToolkits.UI.Controls.MsgBoxSelectors.Models;
using MzIT.BasicToolkits.Utility.Entities;
using MzIT.BasicToolkits.Utility.FetcherHelpers;
using System;
using System.Windows;
using System.Windows.Controls;

namespace MzIT.BasicToolkits.UI.Controls.MsgBoxSelectors
{
    /// <summary>
    /// Interaction logic for MetroMsgBoxSelector.xaml
    /// </summary>
    public partial class MetroMsgBoxSelector : UserControl
    {
        public MetroMsgBoxSelector()
        {
            InitializeComponent();

            InformationDetails = new GME_InformationDetails();
        }

        #region Fields

        private string SpecialStyle { get; set; } = string.Empty;
        private string SpecialImageSourceUri { get; set; } = string.Empty;
        public MetroMsgBoxWindow ParentMetroMsgBoxWindow { get; set; } = null;

        #endregion

        #region Dependency Properties

        #region IsLiteModeItem

        public static readonly DependencyProperty IsLiteModeItemProperty =
            DependencyProperty.Register(
                "IsLiteModeItem",
                typeof(bool),
                typeof(MetroMsgBoxSelector),
                new FrameworkPropertyMetadata(false));

        /// <summary>
        /// GlobalizationDisplayProperties.DisplayProperty_IsEditModeItem;
        /// </summary>
        public bool IsLiteModeItem
        {
            get => (bool)GetValue(IsLiteModeItemProperty);
            set => SetValue(IsLiteModeItemProperty, value);
        }

        #endregion

        #region MetroMsgBoxSelectorType

        public static readonly DependencyProperty MetroMsgBoxSelectorTypeProperty =
            DependencyProperty.Register(
                "MetroMsgBoxSelectorType",
                typeof(LEE_MetroMsgBoxSelectorType),
                typeof(MetroMsgBoxSelector),
                new PropertyMetadata(LEE_MetroMsgBoxSelectorType.None, OnMetroMsgBoxSelectorTypeChanged));

        public LEE_MetroMsgBoxSelectorType MetroMsgBoxSelectorType
        {
            get => (LEE_MetroMsgBoxSelectorType)GetValue(MetroMsgBoxSelectorTypeProperty);
            set => SetValue(MetroMsgBoxSelectorTypeProperty, value);
        }

        private static void OnMetroMsgBoxSelectorTypeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var MetroMsgBoxSelector = (MetroMsgBoxSelector)d;
            var oldMetroMsgBoxSelectorType = (LEE_MetroMsgBoxSelectorType)e.OldValue;
            var newMetroMsgBoxSelectorType = MetroMsgBoxSelector.MetroMsgBoxSelectorType;
            MetroMsgBoxSelector.OnMetroMsgBoxSelectorTypeChanged(oldMetroMsgBoxSelectorType, newMetroMsgBoxSelectorType);
        }

        protected virtual void OnMetroMsgBoxSelectorTypeChanged(LEE_MetroMsgBoxSelectorType oldMetroMsgBoxSelectorType, LEE_MetroMsgBoxSelectorType newMetroMsgBoxSelectorType)
        {
            SetMetroMsgBoxSelectorProperties();
        }

        #endregion

        #region HeaderBrush

        public static readonly DependencyProperty HeaderBrushProperty =
            DependencyProperty.Register(
                "HeaderBrush",
                typeof(string),
                typeof(MetroMsgBoxSelector),
                new FrameworkPropertyMetadata("#FF616161"));

        public string HeaderBrush
        {
            get => (string)GetValue(HeaderBrushProperty);
            set => SetValue(HeaderBrushProperty, value);
        }

        #endregion

        #region DisplayTitle

        public static readonly DependencyProperty DisplayTitleProperty =
            DependencyProperty.Register(
                "DisplayTitle",
                typeof(string),
                typeof(MetroMsgBoxSelector),
                new FrameworkPropertyMetadata(string.Empty));

        public string DisplayTitle
        {
            get => (string)GetValue(DisplayTitleProperty);
            set => SetValue(DisplayTitleProperty, value);
        }

        #endregion

        #region Caption

        public static readonly DependencyProperty CaptionProperty =
            DependencyProperty.Register(
                "Caption",
                typeof(string),
                typeof(MetroMsgBoxSelector),
                new FrameworkPropertyMetadata(string.Empty));

        public string Caption
        {
            get => (string)GetValue(CaptionProperty);
            set => SetValue(CaptionProperty, value);
        }

        #endregion

        #region Message

        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.Register(
                "Message",
                typeof(string),
                typeof(MetroMsgBoxSelector),
                new FrameworkPropertyMetadata(string.Empty));

        public string Message
        {
            get => (string)GetValue(MessageProperty);
            set => SetValue(MessageProperty, value);
        }

        #endregion

        #region Result

        public static readonly DependencyProperty ResultProperty =
            DependencyProperty.Register(
                "Result",
                typeof(string),
                typeof(MetroMsgBoxSelector),
                new FrameworkPropertyMetadata(string.Empty));

        public string Result
        {
            get => (string)GetValue(ResultProperty);
            set => SetValue(ResultProperty, value);
        }

        #endregion

        #region InformationDetails

        public static readonly DependencyProperty InformationDetailsProperty =
            DependencyProperty.Register(
                "InformationDetails",
                typeof(GME_InformationDetails),
                typeof(MetroMsgBoxSelector),
                new PropertyMetadata(new GME_InformationDetails()));

        public GME_InformationDetails InformationDetails
        {
            get => (GME_InformationDetails)GetValue(InformationDetailsProperty);
            set => SetValue(InformationDetailsProperty, value);
        }

        #endregion

        #region DisplayStyle

        public static readonly DependencyProperty DisplayStyleProperty =
            DependencyProperty.Register(
                "DisplayStyle",
                typeof(LEE_MetroMsgBoxSelectorDisplayStyle),
                typeof(MetroMsgBoxSelector),
                new FrameworkPropertyMetadata(LEE_MetroMsgBoxSelectorDisplayStyle.Text));

        public LEE_MetroMsgBoxSelectorDisplayStyle DisplayStyle
        {
            get => (LEE_MetroMsgBoxSelectorDisplayStyle)GetValue(DisplayStyleProperty);
            set => SetValue(DisplayStyleProperty, value);
        }

        #endregion

        #region IsDefaultProperties

        public static readonly DependencyProperty IsDefaultPropertiesProperty =
            DependencyProperty.Register(
                "IsDefaultProperties",
                typeof(bool),
                typeof(MetroMsgBoxSelector),
                new FrameworkPropertyMetadata(true, OnIsDefaultPropertiesChanged));

        public bool IsDefaultProperties
        {
            get => (bool)GetValue(IsDefaultPropertiesProperty);
            set => SetValue(IsDefaultPropertiesProperty, value);
        }

        private static void OnIsDefaultPropertiesChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var MetroMsgBoxSelector = (MetroMsgBoxSelector)d;
            var oldIsDefaultProperties = (bool)e.OldValue;
            var newIsDefaultProperties = MetroMsgBoxSelector.IsDefaultProperties;
            MetroMsgBoxSelector.OnIsDefaultPropertiesChanged(oldIsDefaultProperties, newIsDefaultProperties);
        }

        protected virtual void OnIsDefaultPropertiesChanged(bool oldIsDefaultProperties, bool newIsDefaultProperties)
        {
            SetMetroMsgBoxSelectorProperties();
        }

        #endregion

        #region MetroMessageBoxButton

        public static readonly DependencyProperty MetroMessageBoxButtonProperty =
            DependencyProperty.Register(
                "MetroMessageBoxButton",
                typeof(Nullable<MessageBoxButton>),
                typeof(MetroMsgBoxSelector),
                new PropertyMetadata(null, OnMetroMessageBoxButtonChanged));

        public Nullable<MessageBoxButton> MetroMessageBoxButton
        {
            get => (Nullable<MessageBoxButton>)GetValue(MetroMessageBoxButtonProperty);
            set => SetValue(MetroMessageBoxButtonProperty, value);
        }

        private static void OnMetroMessageBoxButtonChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var MetroMsgBoxSelector = (MetroMsgBoxSelector)d;
            var oldMetroMessageBoxButton = (Nullable<MessageBoxButton>)e.OldValue;
            var newMetroMessageBoxButton = MetroMsgBoxSelector.MetroMessageBoxButton;
            MetroMsgBoxSelector.OnMetroMessageBoxButtonChanged(oldMetroMessageBoxButton, newMetroMessageBoxButton);
        }

        protected virtual void OnMetroMessageBoxButtonChanged(Nullable<MessageBoxButton> oldMetroMessageBoxButton, Nullable<MessageBoxButton> newMetroMessageBoxButton)
        {
            SetMetroMsgBoxSelectorProperties();
        }

        #endregion

        #region MetroMessageBoxImage

        public static readonly DependencyProperty MetroMessageBoxImageProperty =
            DependencyProperty.Register(
                "MetroMessageBoxImage",
                typeof(Nullable<LEE_MetroMsgBoxSelectorType>),
                typeof(MetroMsgBoxSelector),
                new PropertyMetadata(null, OnMetroMessageBoxImageChanged));

        public Nullable<LEE_MetroMsgBoxSelectorType> MetroMessageBoxImage
        {
            get => (Nullable<LEE_MetroMsgBoxSelectorType>)GetValue(MetroMessageBoxImageProperty);
            set => SetValue(MetroMessageBoxImageProperty, value);
        }

        private static void OnMetroMessageBoxImageChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var MetroMsgBoxSelector = (MetroMsgBoxSelector)d;
            var oldMetroMessageBoxImage = (Nullable<LEE_MetroMsgBoxSelectorType>)e.OldValue;
            var newMetroMessageBoxImage = MetroMsgBoxSelector.MetroMessageBoxImage;
            MetroMsgBoxSelector.OnMetroMessageBoxImageChanged(oldMetroMessageBoxImage, newMetroMessageBoxImage);
        }

        protected virtual void OnMetroMessageBoxImageChanged(Nullable<LEE_MetroMsgBoxSelectorType> oldMetroMessageBoxImage, Nullable<LEE_MetroMsgBoxSelectorType> newMetroMessageBoxImage)
        {
            SetMetroMsgBoxSelectorProperties();
        }

        #endregion

        #region DefaultMessageBoxResult

        public static readonly DependencyProperty DefaultMessageBoxResultProperty =
            DependencyProperty.Register(
                "DefaultMessageBoxResult",
                typeof(Nullable<MessageBoxResult>),
                typeof(MetroMsgBoxSelector),
                new PropertyMetadata(null, OnDefaultMessageBoxResultChanged));

        public Nullable<MessageBoxResult> DefaultMessageBoxResult
        {
            get => (Nullable<MessageBoxResult>)GetValue(DefaultMessageBoxResultProperty);
            set => SetValue(DefaultMessageBoxResultProperty, value);
        }

        private static void OnDefaultMessageBoxResultChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var MetroMsgBoxSelector = (MetroMsgBoxSelector)d;
            var oldDefaultMessageBoxResult = (Nullable<MessageBoxResult>)e.OldValue;
            var newDefaultMessageBoxResult = MetroMsgBoxSelector.DefaultMessageBoxResult;
            MetroMsgBoxSelector.OnDefaultMessageBoxResultChanged(oldDefaultMessageBoxResult, newDefaultMessageBoxResult);
        }

        protected virtual void OnDefaultMessageBoxResultChanged(Nullable<MessageBoxResult> oldDefaultMessageBoxResult, Nullable<MessageBoxResult> newDefaultMessageBoxResult)
        {
            SetMetroMsgBoxSelectorProperties();
        }

        #endregion

        #region ImageSourceUri

        public static readonly DependencyProperty ImageSourceUriProperty =
            DependencyProperty.Register(
                "ImageSourceUri",
                typeof(string),
                typeof(MetroMsgBoxSelector),
                new FrameworkPropertyMetadata(string.Empty));

        internal string ImageSourceUri
        {
            get => (string)GetValue(ImageSourceUriProperty);
            set => SetValue(ImageSourceUriProperty, value);
        }

        #endregion

        #endregion

        #region Common Methods

        internal void UserControl_Loaded()
        {
            try
            {

            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex); }
        }

        internal void UserControl_Unloaded()
        {
            try
            {

            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex); }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Refresh MetroMsgBoxSelectorType and Set Properties
        /// </summary>
        private void SetMetroMsgBoxSelectorProperties()
        {
            try
            {
                MessageBoxButton? button = MetroMessageBoxButton;
                LEE_MetroMsgBoxSelectorType? image = MetroMessageBoxImage;
                MessageBoxResult? defaultResult = DefaultMessageBoxResult;

                if (IsDefaultProperties
                    && MetroMsgBoxSelectorType != LEE_MetroMsgBoxSelectorType.Custom)
                {
                    SpecialStyle = string.Empty;
                    SpecialImageSourceUri = string.Empty;
                    ImageSourceUri = string.Empty;

                    switch (MetroMsgBoxSelectorType)
                    {
                        #region Default Types

                        case LEE_MetroMsgBoxSelectorType.None:
                            {
                                if (button == null)
                                { button = MessageBoxButton.OK; }
                                if (image == null)
                                { image = MetroMsgBoxSelectorType; }
                                if (defaultResult == null)
                                { defaultResult = MessageBoxResult.OK; }

                                break;
                            }
                        case LEE_MetroMsgBoxSelectorType.Hand:
                            {
                                if (button == null)
                                { button = MessageBoxButton.OK; }
                                if (image == null)
                                { image = MetroMsgBoxSelectorType; }
                                if (defaultResult == null)
                                { defaultResult = MessageBoxResult.OK; }

                                break;
                            }
                        case LEE_MetroMsgBoxSelectorType.Stop:
                            {
                                if (button == null)
                                { button = MessageBoxButton.OK; }
                                if (image == null)
                                { image = MetroMsgBoxSelectorType; }
                                if (defaultResult == null)
                                { defaultResult = MessageBoxResult.OK; }

                                break;
                            }
                        case LEE_MetroMsgBoxSelectorType.Error:
                            {
                                if (button == null)
                                { button = MessageBoxButton.OK; }
                                if (image == null)
                                { image = MetroMsgBoxSelectorType; }
                                if (defaultResult == null)
                                { defaultResult = MessageBoxResult.OK; }

                                break;
                            }
                        case LEE_MetroMsgBoxSelectorType.Question:
                            {
                                if (button == null)
                                { button = MessageBoxButton.YesNo; }
                                if (image == null)
                                { image = MetroMsgBoxSelectorType; }
                                if (defaultResult == null)
                                { defaultResult = MessageBoxResult.Yes; }

                                break;
                            }
                        case LEE_MetroMsgBoxSelectorType.Exclamation:
                            {
                                if (button == null)
                                { button = MessageBoxButton.OK; }
                                if (image == null)
                                { image = MetroMsgBoxSelectorType; }
                                if (defaultResult == null)
                                { defaultResult = MessageBoxResult.OK; }

                                break;
                            }
                        case LEE_MetroMsgBoxSelectorType.Warning:
                            {
                                if (button == null)
                                { button = MessageBoxButton.OK; }
                                if (image == null)
                                { image = MetroMsgBoxSelectorType; }
                                if (defaultResult == null)
                                { defaultResult = MessageBoxResult.OK; }

                                break;
                            }
                        case LEE_MetroMsgBoxSelectorType.Asterisk:
                            {
                                if (button == null)
                                { button = MessageBoxButton.OK; }
                                if (image == null)
                                { image = MetroMsgBoxSelectorType; }
                                if (defaultResult == null)
                                { defaultResult = MessageBoxResult.OK; }

                                break;
                            }
                        case LEE_MetroMsgBoxSelectorType.Information:
                            {
                                if (button == null)
                                { button = MessageBoxButton.OK; }
                                if (image == null)
                                { image = MetroMsgBoxSelectorType; }
                                if (defaultResult == null)
                                { defaultResult = MessageBoxResult.OK; }

                                break;
                            }

                        #endregion

                        #region Special

                        case LEE_MetroMsgBoxSelectorType.TryCatchError:
                            {
                                if (button == null)
                                { button = MessageBoxButton.OK; }
                                if (image == null)
                                { image = MetroMsgBoxSelectorType; }
                                if (defaultResult == null)
                                { defaultResult = MessageBoxResult.OK; }

                                break;
                            }
                        case LEE_MetroMsgBoxSelectorType.ValidationError:
                            {
                                if (button == null)
                                { button = MessageBoxButton.OK; }
                                if (image == null)
                                { image = MetroMsgBoxSelectorType; }
                                if (defaultResult == null)
                                { defaultResult = MessageBoxResult.OK; }

                                break;
                            }
                        case LEE_MetroMsgBoxSelectorType.Success:
                            {
                                if (button == null)
                                { button = MessageBoxButton.OK; }
                                if (image == null)
                                { image = MetroMsgBoxSelectorType; }
                                if (defaultResult == null)
                                { defaultResult = MessageBoxResult.OK; }

                                break;
                            }
                        case LEE_MetroMsgBoxSelectorType.Confirm:
                            {
                                if (button == null)
                                { button = MessageBoxButton.OK; }
                                if (image == null)
                                { image = MetroMsgBoxSelectorType; }
                                if (defaultResult == null)
                                { defaultResult = MessageBoxResult.OK; }

                                break;
                            }
                        case LEE_MetroMsgBoxSelectorType.Failure:
                            {
                                if (button == null)
                                { button = MessageBoxButton.OK; }
                                if (image == null)
                                { image = MetroMsgBoxSelectorType; }
                                if (defaultResult == null)
                                { defaultResult = MessageBoxResult.OK; }

                                break;
                            }
                        case LEE_MetroMsgBoxSelectorType.Print:
                            {
                                if (button == null)
                                { button = MessageBoxButton.OK; }
                                if (image == null)
                                { image = MetroMsgBoxSelectorType; }
                                if (defaultResult == null)
                                { defaultResult = MessageBoxResult.OK; }

                                break;
                            }

                        #endregion

                        #region Combine

                        case LEE_MetroMsgBoxSelectorType.QuestionStop:
                            {
                                if (button == null)
                                { button = MessageBoxButton.YesNo; }
                                if (image == null)
                                { image = MetroMsgBoxSelectorType; }
                                if (defaultResult == null)
                                { defaultResult = MessageBoxResult.No; }

                                break;
                            }
                        case LEE_MetroMsgBoxSelectorType.QuestionWarning:
                            {
                                if (button == null)
                                { button = MessageBoxButton.YesNo; }
                                if (image == null)
                                { image = MetroMsgBoxSelectorType; }
                                if (defaultResult == null)
                                { defaultResult = MessageBoxResult.No; }

                                break;
                            }
                        case LEE_MetroMsgBoxSelectorType.QuestionConfirm:
                            {
                                if (button == null)
                                { button = MessageBoxButton.YesNo; }
                                if (image == null)
                                { image = MetroMsgBoxSelectorType; }
                                if (defaultResult == null)
                                { defaultResult = MessageBoxResult.Yes; }

                                break;
                            }
                        case LEE_MetroMsgBoxSelectorType.QuestionDelete:
                            {
                                if (button == null)
                                { button = MessageBoxButton.YesNo; }
                                if (image == null)
                                { image = MetroMsgBoxSelectorType; }
                                if (defaultResult == null)
                                { defaultResult = MessageBoxResult.No; }

                                break;
                            }
                        case LEE_MetroMsgBoxSelectorType.QuestionPrint:
                            {
                                if (button == null)
                                { button = MessageBoxButton.YesNo; }
                                if (image == null)
                                { image = MetroMsgBoxSelectorType; }
                                if (defaultResult == null)
                                { defaultResult = MessageBoxResult.Yes; }

                                break;
                            }

                        #endregion

                        #region Custom by user

                        case LEE_MetroMsgBoxSelectorType.Custom:
                            {
                                if (button == null)
                                { button = MessageBoxButton.OK; }
                                if (image == null)
                                { image = LEE_MetroMsgBoxSelectorType.None; }
                                if (defaultResult == null)
                                { defaultResult = MessageBoxResult.OK; }

                                break;
                            }

                            #endregion
                    }
                }

                if (button == null)
                { button = MessageBoxButton.OK; }
                if (image == null)
                { image = LEE_MetroMsgBoxSelectorType.None; }
                if (defaultResult == null)
                { defaultResult = MessageBoxResult.OK; }

                HeaderBrush = MetroMsgBoxSelectorType.ToSpecialColorString();
                DisplayTitle = MetroMsgBoxSelectorType.ToDescriptionString();

                #region Set MetroMessageBoxButton

                buttonOK.Visibility = Visibility.Collapsed;
                buttonYes.Visibility = Visibility.Collapsed;
                buttonNo.Visibility = Visibility.Collapsed;
                buttonCancel.Visibility = Visibility.Collapsed;

                switch (button)
                {
                    case MessageBoxButton.OK:
                        {
                            buttonOK.Visibility = Visibility.Visible;

                            break;
                        }
                    case MessageBoxButton.OKCancel:
                        {
                            buttonOK.Visibility = Visibility.Visible;
                            buttonCancel.Visibility = Visibility.Visible;

                            break;
                        }
                    case MessageBoxButton.YesNo:
                        {
                            buttonYes.Visibility = Visibility.Visible;
                            buttonNo.Visibility = Visibility.Visible;

                            break;
                        }
                    case MessageBoxButton.YesNoCancel:
                        {
                            buttonYes.Visibility = Visibility.Visible;
                            buttonNo.Visibility = Visibility.Visible;
                            buttonCancel.Visibility = Visibility.Visible;

                            break;
                        }
                }

                #endregion

                #region Set DefaultMessageBoxResult

                buttonOK.IsDefault = false;
                buttonYes.IsDefault = false;
                buttonNo.IsDefault = false;
                buttonCancel.IsDefault = false;

                switch (defaultResult)
                {
                    case MessageBoxResult.OK:
                        {
                            if (buttonOK.Visibility == Visibility.Visible)
                            {
                                //buttonOK.IsDefault = true;
                                buttonOK.Focus();
                            }

                            break;
                        }
                    case MessageBoxResult.Cancel:
                        {
                            if (buttonCancel.Visibility == Visibility.Visible)
                            {
                                //buttonCancel.IsDefault = true;
                                buttonCancel.Focus();
                            }

                            break;
                        }
                    case MessageBoxResult.Yes:
                        {
                            if (buttonYes.Visibility == Visibility.Visible)
                            {
                                //buttonYes.IsDefault = true;
                                buttonYes.Focus();
                            }

                            break;
                        }
                    case MessageBoxResult.No:
                        {
                            if (buttonNo.Visibility == Visibility.Visible)
                            {
                                //buttonNo.IsDefault = true;
                                buttonNo.Focus();
                            }

                            break;
                        }
                }

                #endregion

                #region Set MetroMessageBoxImage

                if (image == LEE_MetroMsgBoxSelectorType.None || image == LEE_MetroMsgBoxSelectorType.Custom)
                { DisplayStyle = LEE_MetroMsgBoxSelectorDisplayStyle.Text; }
                else
                { DisplayStyle = LEE_MetroMsgBoxSelectorDisplayStyle.ImageAndText; }

                SpecialImageSourceUri = image.ToSpecialDescriptionString();
                if (SpecialImageSourceUri.Trim() != "")
                {
                    ImageSourceUri =
                        (new Uri(
                            string.Format(
                                "{0}{1}",
                                "pack://application:,,,/MzIT.BasicToolkits.;component/UI/Controls/MsgBoxSelectors/Images/",
                                SpecialImageSourceUri))).ToString();
                }

                #endregion

                #region Set DefaultMessageBoxResult

                #endregion
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }
        }

        private void UserControlMove_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                if (e.ChangedButton == System.Windows.Input.MouseButton.Left && ParentMetroMsgBoxWindow != null)
                { ParentMetroMsgBoxWindow.DragMove(); }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }
        }

        private void ButtonOK_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (buttonOK.Visibility != Visibility.Visible)
                { return; }

                SpecialModels.HideHandlerDialog(this, true);
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }
        }

        private void ButtonYes_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (buttonYes.Visibility != Visibility.Visible)
                { return; }

                SpecialModels.HideHandlerDialog(this, true);
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }
        }

        private void ButtonNo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (buttonNo.Visibility != Visibility.Visible)
                { return; }

                SpecialModels.HideHandlerDialog(this, false);
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (buttonCancel.Visibility != Visibility.Visible)
                { return; }

                SpecialModels.HideHandlerDialog(this, false);
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }
        }

        #endregion

        /// <summary>
        /// Load this UserControl
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {

            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.BasicModelsPermission); }
        }
    }
}
