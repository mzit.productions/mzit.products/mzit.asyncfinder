﻿using MzIT.BasicToolkits.Utility.Entities;

namespace MzIT.AsyncFinder
{
    internal static class FetcherSingleInstanceLogic
    {
        #region Controlling Fields

        internal static bool IsSetToSingleInstanceApp { get; set; } = true;

        internal static bool IsSetDefaultProperties { get; set; } = false;
        public static GME_InternetConnectionStatus InternetConnectionStatus { get; set; } = new GME_InternetConnectionStatus();

        internal static bool KillRunningProcesses { get; set; } = true;

        #endregion

        #region Condition Fields

        #endregion

        #region Properties+Functions

        #endregion
    }
}
