﻿using MzIT.BasicToolkits.Utility.FetcherHelpers;
using MzIT.BasicToolkits.Utility.MultiThreadedUtility;
using MzIT.DataAccess.FetcherControllers;
using System;
using System.Collections.Generic;
using System.Linq;
using static MzIT.BasicToolkits.Utility.MultiThreadedUtility.FetcherMultiThreadedUtility;

namespace MzIT.AsyncFinder.Utility.MultiThreadedFunctions
{
    internal class MTF_DetectionSystem
    {
        public MTF_DetectionSystem(string searchValue)
        {
            SearchValue = searchValue;
            SetMultiThreadedPeriodicFunctionsDetails_DetectionSystem(true);
        }

        public MTF_DetectionSystem(string searchValue, int part, List<string> inputPaths)
        {
            SearchValue = searchValue;
            Part = part;
            InputPaths = inputPaths;
        }

        #region Feilds

        private string _searchValue;
        public string SearchValue
        {
            get => _searchValue;
            set { _searchValue = value?.Trim(); }
        }

        private List<string> _inputFiles { get; set; }
        private IEnumerable<List<string>> _splitListInputPaths { get; set; }

        public int Part { get; set; }
        private List<string> InputPaths { get; set; }

        public int SplitListNSize { get; set; } = 100;

        #endregion

        #region MultiThreadedPeriodicFunctions - DetectionSystem

        #region MultiThreadedPeriodicFunctions Fields

        /// <summary>
        /// In app, this is a member variable of the main window
        /// </summary>
        internal MultiThreadedPeriodicFunctions MTPF_DetectionSystem { get; set; }

        #endregion

        #region MultiThreadedPeriodicFunctions Methods

        /// <summary>
        /// Set MultiThreadedPeriodicFunctions Details for this application
        /// </summary>
        /// <param name="IsAutoActive"></param>
        internal void SetMultiThreadedPeriodicFunctionsDetails_DetectionSystem(bool IsAutoActive)
        {
            try
            {
                if (MTPF_DetectionSystem == null)
                {
                    MTPF_DetectionSystem = new MultiThreadedPeriodicFunctions
                    {
                        NextAuthorizationThreshold = new NextAuthorizationThreshold(LEE_NextAuthorizationThresholdType.Report)
                    };

                    MTPF_DetectionSystem.OnActive_Tick += new System.Timers.ElapsedEventHandler(MTPF_DispatcherTimerProcess_OnActive_Tick_DetectionSystem);
                }

                if (IsAutoActive)
                { MTPF_DetectionSystem.MultiThreadedPeriodicFunctions_ActiveMTPF(true); }
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex); }
        }

        /// <summary>
        /// Do something works in a period in special thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MTPF_DispatcherTimerProcess_OnActive_Tick_DetectionSystem(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                FetcherMultiThreadedUtility.MTF_StartThread_Invoke(MTPF_TimerCounterProcess_OnActive_DetectionSystem);
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex); }
        }

        /// <summary>
        /// Counter of this timer
        /// Check state in special period and do something if NextAuthorizationThreshold.IsEnabledCore==True
        /// Update PeriodicFunctions from server in special thread separate of main application
        /// </summary>
        private void MTPF_TimerCounterProcess_OnActive_DetectionSystem()
        {
            try
            {
                if (MTPF_DetectionSystem.IsDisabledRunFunction())
                { return; }

                MTPF_RunPeriodicFunctions_DetectionSystem(true);
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.FullPermission, FetcherTryCatch.FTC_PermissionType.DoNotLogError); }
        }

        /// <summary>
        /// Refresh DetectionSystem
        /// Check Network Connection if Refresh DataSource
        /// </summary>
        /// <param name="IsRefreshDataSource"></param>
        /// <returns></returns>
        internal bool MTPF_RunPeriodicFunctions_DetectionSystem(bool IsRefreshDataSource)
        {
            bool IsValid = true;

            try
            {
                if (MTPF_DetectionSystem.BeginRunFunction(IsRefreshDataSource))
                {
                    Logging.LogAction("\n\n\n", false);
                    Logging.LogAction("BeginRunFunction;", true);

                    _inputFiles = ArticlesController.GetInputFiles();
                    Logging.LogAction($"The number of incoming files is {_inputFiles?.Count};", true);

                    _splitListInputPaths = IEnumerableExtension.SplitList<string>(_inputFiles, SplitListNSize);
                    Logging.LogAction($"Split list entry paths by {_splitListInputPaths?.Count()};", true);

                    int index = 0;
                    foreach (var currentInputPaths in _splitListInputPaths)
                    {
                        if (!currentInputPaths.IsNullOrEmpty())
                        {
                            GetArticlesPartBy(currentInputPaths, ++index);
                        }
                    }

                    MTPF_DetectionSystem.EndRunFunction();
                }
                else { MTPF_DetectionSystem.EndRunFunction(); }
                Logging.LogAction("\n\n", false);
                Logging.LogAction("EndRunFunction;\n\n", true);
            }
            catch (Exception ex)
            {
                IsValid = false;
                MTPF_DetectionSystem.EndRunFunction();
                FetcherTryCatch.FTC_GetCEM_Error(ex, FetcherTryCatch.FTC_PermissionType.FullPermission);
            }

            return IsValid;
        }

        #endregion

        #endregion

        #region Helper Methods

        private async void GetArticlesPartBy(List<string> currentInputPaths, int part)
        {
            MTF_DetectionSystem oMTF_DetectionSystem = new MTF_DetectionSystem(SearchValue, part, currentInputPaths);
            oMTF_DetectionSystem.Dispatcher_MTFTick_SearchingArticlesPartBy();
        }

        #endregion

        #region Methods

        private void SearchingDetectionSystem()
        {
            MTPF_DetectionSystem.NextAuthorizationThreshold.ResetAndActive();
        }

        private void SearchingArticlesPartBy()
        {
            Logging.LogAction("\n", false);
            Logging.LogAction($"Begin Part #{Part};\n", true);

            ArticlesController.GetArticles(InputPaths, SearchValue);
        }

        #endregion

        #region Dispatcher_MTFTick Methods

        private void Dispatcher_MTFTick_StartThread(MTF_Method method)
            => FetcherMultiThreadedUtility.MTF_StartThread_Invoke(method, true, System.Threading.ThreadPriority.Highest);

        public void Dispatcher_MTFTick_SearchingDetectionSystem() => Dispatcher_MTFTick_StartThread(SearchingDetectionSystem);
        public void Dispatcher_MTFTick_SearchingArticlesPartBy() => Dispatcher_MTFTick_StartThread(SearchingArticlesPartBy);

        #endregion
    }
}
