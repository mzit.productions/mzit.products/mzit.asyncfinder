﻿using MzIT.AsyncFinder.Utility.MultiThreadedFunctions;
using MzIT.BasicToolkits.Utility.FetcherHelpers;
using System;

namespace MzIT.AsyncFinder
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Logging.IsDebugMode = false;
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            try
            {
                #region Hedear

                Logging.LogAction("\n\n\t[MzIT.AsyncFinder]", false);
                Logging.LogAction("\n\tA simple project to use of threading algorithm for multi threading tasks;", false);
                Logging.LogAction("\tFind the key search in a huge amount of data;\n", false);

                Logging.LogAction("\tPlease for run 'Detection System' press a key:\n", false);
                Console.ReadKey();

                #endregion

                #region Run report

                string searchValue = string.Empty;

                while (string.IsNullOrWhiteSpace(searchValue))
                {
                    Logging.LogAction("\tPlease enter your keyword to start the search engine:\n", false);
                    searchValue = Console.ReadLine();
                }

                MTF_DetectionSystem oMTF_DetectionSystem = new MTF_DetectionSystem(searchValue);
                oMTF_DetectionSystem.Dispatcher_MTFTick_SearchingDetectionSystem();

                #endregion

                #region Footer

                bool closeApp = false;
                string input = string.Empty;

                while (!closeApp)
                {
                    Logging.LogAction("\n\n\tSearching detection system is running in background.", false);
                    Logging.LogAction("\n\tIf you want to end the program, please enter 'TRUE'.", false);
                    input = Console.ReadLine();

                    if (input?.Trim()?.ToUpper() == true.ToString().ToUpper())
                    { closeApp = true; }
                }

                Logging.LogAction("\n\n\tGood bye.", false);
                Logging.LogAction("\tEnd program.", false);
                Logging.LogAction("\n\t--------------\t", false);
                Console.ReadKey();

                #endregion
            }
            catch (Exception ex) { FetcherTryCatch.FTC_GetCEM_Error(ex); }
        }
    }
}
